import ApplicationModel from './../application-model';
import ScoreModel from "./score-model";

export default class TestRunScoreModel extends ApplicationModel{
  /**
   * Standard Test Score
   * @type {ScoreModel}
   */
  standardScore;

  /**
   * Hash with different key, each one associated to a
   * psychosomatic category present in test questions
   * and the respective score based on the answers
   * @type {Object}
   */
  psychosometricScore;

  /**
   *
   * @param {TestRunModel} testRun
   */
  constructor(testRun) {
    super();

    this.standardScore = this._standardScore(testRun);
    this.psychosometricScore = this._psychosometricScore(testRun);
  }

  /**
   *
   * @param {TestRunAnswerModel} testRunAnswer
   * @return {ScoreModel}
   * @private
   */
  _answerScore(testRunAnswer) {
    return new ScoreModel(
      testRunAnswer.scoreValue,
      testRunAnswer.minimumScoreValue,
      testRunAnswer.maximumScoreValue);
  }

  /**
   *
   * @param {Array[ScoreModel]} scores
   * @return {ScoreModel}
   * @private
   */
  _totalScore(scores) {
    return scores.reduce((s, acc) => {
      return new ScoreModel(
        s.scoreValue + acc.scoreValue,
        s.minimumScoreValue + acc.minimumScoreValue,
        s.maximumScoreValue + acc.maximumScoreValue
      )
    }, new ScoreModel(0, 0, 0))
  }

  /**
   *
   * @param {TestRunModel} testRun
   * @return {ScoreModel}
   * @private
   */
  _standardScore(testRun) {
    const testRunAnswerScores = testRun.testRunAnswers.map((tra) => this._answerScore(tra))
    return this._totalScore(testRunAnswerScores)
  }

  /**
   *
   * @param {TestRunModel} testRun
   * @return {Object}
   * @private
   */
  _psychosometricScore(testRun) {
    let score = {}
    testRun.testCategories.forEach((c) => {
      const testRunAnswerScores = testRun.testRunCategoryAnswers(c).map((tra) => this._answerScore(tra))
      score[c] = this._totalScore(testRunAnswerScores)
    })
    return score
  }

  /**
   * @return {ScoreModel}
   */
  get standardScore() {
    return this.standardScore;
  }

  /**
   * @return {number}
   */
  get standardScoreValue() {
    return this.standardScore.scoreValue;
  }

  /**
   * @return {Object}
   */
  get psychosometricScore() {
    return this.psychosometricScore;
  }

  /**
   * return {Object}
   */
  get normalizedPsychosometricScore() {
    const psychosometricScore = this.psychosometricScore;
    const parametrizedPS = {}
    Object.keys(psychosometricScore).forEach((k) =>
      parametrizedPS[k] = psychosometricScore[k].normalizedScoreValue)
    return parametrizedPS;
  }
}
