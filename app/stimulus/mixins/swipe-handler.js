import raf from 'raf';
import Hammer from 'hammerjs';
import rebound from 'rebound';
import { isTouchDevice } from '../../utils/common/device-methods';
import { triggerCustomEvent } from '../../utils/common/events-methods';
import {
  cardSpringConfiguration,
  springCard,
} from '../../utils/gameplay/spring-methods';
import {
  cardHammerConfiguration,
  moveCard,
} from '../../utils/gameplay/drag-methods';
import {
  calculateDirectionDestination,
  calculateDirection,
} from '../../utils/gameplay/swipe-direction-methods';

/**
 * Stimulus Controller Mixin for implementing game swipe mechanics
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const swipeHandler = (controller) => {
  Object.assign(controller, {
    /**
     * Determines whether card has been dragged enough to be thrown out (confidence 1)
     * or thrown in (confidence lower than 1). This is the default card logic, which
     * can be overwritten by specifying another method in the card initializer
     *
     * @param {HTMLElement} cardElement
     * @param {Number} movementOffsetX
     * @param {Number} movementOffsetY
     * @return {number}
     * @private
     */
    _defaultDragConfidence(cardElement, movementOffsetX, movementOffsetY) {
      const xConfidence = Math.min(Math.abs(movementOffsetX) / (0.15 * cardElement.offsetWidth), 1);
      const yConfidence = Math.min(Math.abs(movementOffsetY) / (0.15 * cardElement.offsetHeight), 1);
      return Math.max(xConfidence, yConfidence);
    },

    _allowedCardDirection(direction) {
      if(this.hasModelValue) return(this.modelValue?.directions || [direction]).includes(direction)
      return true
    },

    _canThrow(cardElement, movementOffsetX, movementOffsetY) {
      const throwConfidence = this._defaultDragConfidence(this.element, this.lastCardOffsetX, this.lastCardOffsetY)
      const throwDirection = calculateDirection(this.allowedSwipeDirections, movementOffsetX, movementOffsetY)
      return throwConfidence === 1 && this.canThrow && this._allowedCardDirection(throwDirection)
    },

    /**
     * Create Hammer.js manager and assign callback logic for the
     * different swipe stages (start, end and on movement)
     *
     * @return {Manager}
     * @private
     */
    _hammerConfiguration() {
      return cardHammerConfiguration(
        this.element,
        1,
        /**
         * Initialize swipe parameters and trigger movement frame animation
         * @param event
         */
        (event) => {
          this.swiping = true;
          this.swipeOffsetX = 0;
          this.swipeOffsetY = 0;

          const controller = this;

          triggerCustomEvent('cardGrabbed', this.element, true, this);

          // raf creates an animation loop which calls
          // the animation method until animation is over
          (function animation() {
            if (controller.swiping && controller.canSwipe) {
              moveCard(
                controller.element,
                controller.lastCardOffsetX,
                controller.lastCardOffsetY,
                controller.swipeOffsetX,
                controller.swipeOffsetY
              );
              raf(animation);
            }
          })();
        },
        /**
         * While swiping update swipe movement position. Animator will read
         * new value on each animation frame updating the card position
         * @param event
         */
        (event) => {
          this.swipeOffsetX = event.deltaX;
          this.swipeOffsetY = event.deltaY;

          // Send movement event with information about direction and confidence
          triggerCustomEvent('cardDragged', this.element, true, {
            confidence: this._defaultDragConfidence(this.element, this.swipeOffsetX, this.swipeOffsetY),
            direction: calculateDirection(this.allowedSwipeDirections, this.swipeOffsetX, this.swipeOffsetY)
          });
        },
        /**
         * Update swipe parameters as stopped and depending
         * on movement confidence throw in or out card
         * @param event
         */
        (event) => {
          this.swiping = false;

          this.lastCardOffsetX = this.lastCardOffsetX + event.deltaX;
          this.lastCardOffsetY = this.lastCardOffsetY + event.deltaY;

          triggerCustomEvent('cardDropped', this.element, true, this)

          if (this._canThrow(this.element, this.lastCardOffsetX, this.lastCardOffsetY)) {
            this.throwOut(this.lastCardOffsetX, this.lastCardOffsetY);
          } else {
            this.throwIn(this.lastCardOffsetX, this.lastCardOffsetY);
          }
        },
        () => this.canSwipe
      );
    },

    /**
     * Initialize spring systems to throw in and out cards
     *
     * @return {[Spring,Spring]}
     * @private
     */
    _springConfiguration() {
      return cardSpringConfiguration(
        100,
        20,
        500,
        150,
        /**
         * When card is thrown in spring animation is triggered transforming the card
         * offset from its current value to 0, which translates in returning the card
         * to the deck
         *
         * @param spring
         */
        (spring) => {
          [this.lastCardOffsetX, this.lastCardOffsetY] = springCard(
            spring,
            this.element,
            this.lastCardOffsetX,
            this.lastCardOffsetY,
            0,
            0
          );
        },
        /**
         * When card is thrown out animation is triggered transforming the card
         * offset from its current value to an offset that corresponds to the
         * thrown direction
         *
         * @param spring
         */
        (spring) => {
          [this.lastCardOffsetX, this.lastCardOffsetY] = springCard(
            spring,
            this.element,
            this.lastCardOffsetX,
            this.lastCardOffsetY,
            this.lastDestination[0],
            this.lastDestination[1]
          );
        },
        (spring) => {
          this.canSwipe = true;
          triggerCustomEvent('cardThrownIn', this.element, true, this);
        },
        (spring) => {
          this.canSwipe = true;
          triggerCustomEvent('cardThrownOut', this.element, true, this);
        }
      );
    },

    /**
     * Logic to initialize card swipe mechanic by combining drag and spring
     * mechanics all together
     *
     * @private
     */
    _initializeCard(
      throwConfidenceCalculator = this._defaultDragConfidence,
      allowedSwipeDirections = 4
    ) {
      // Intialize card attributes
      // -------------------------
      this.allowedSwipeDirections = allowedSwipeDirections;
      this.throwConfidenceCalculator = throwConfidenceCalculator;

      // Swipe status
      this.swiping = false;
      this.canSwipe = true;
      this.canThrow = true;

      // Swipe movement offset
      this.swipeOffsetX = 0;
      this.swipeOffsetY = 0;

      // Card last position offset. Updated when user stops
      // dragging and during each frame of the spring animation
      this.lastCardOffsetX = 0;
      this.lastCardOffsetY = 0;

      // Last card selected answer
      this.lastDirection;
      this.lastDestination;

      // Hammer JS drag initializer
      this.hammerInstance = this._hammerConfiguration();

      // In/Out Spring initializer
      const sc = this._springConfiguration();
      this.throwInSpring = sc[0];
      this.throwOutSpring = sc[1];

      // Touch Device Logic
      if (isTouchDevice()) {
        // Even triggers for start & stop touching the screen
        this.element.addEventListener('touchstart', (event) => {
          triggerCustomEvent('panstart', this.element, true, event);
        });

        this.element.addEventListener('touchend', (event) => {
          triggerCustomEvent('panend', this.element, true, event);
        });
        // Non-Touch Device Logic
      } else {
        // Even triggers for start & stop dragging the mouse while clicked
        this.element.addEventListener('mousedown', (event) => {
          triggerCustomEvent('panstart', this.element, true, event);
        });

        this.element.addEventListener('mouseup', (event) => {
          if (this.swiping && !this.panning)
            triggerCustomEvent('panend', this.element, true, event);
        });
      }
    },

    /**
     * Generic throw logic to use the spring system to move a card
     * between a destination and an origin. Implements two logics for
     * throwing cards out of the deck and in the deck.
     *
     * @param direction
     * @param originX
     * @param originY
     * @param direction
     * @private
     */
    _throw(direction, originX, originY) {
      this.canSwipe = false;
      this.lastCardOffsetX = originX;
      this.lastCardOffsetY = originY;

      if (direction) {
        this.lastDirection = direction
        this.lastDestination = calculateDirectionDestination(
          direction,
          this.element,
          this.element.parentElement
        );
        this.throwOutSpring
          .setCurrentValue(0)
          .setAtRest()
          .setVelocity(100)
          .setEndValue(1);
        triggerCustomEvent('cardStartingThrownOut', this.element, true, this)
      } else {
        this.throwInSpring.setCurrentValue(0).setAtRest().setEndValue(1);
        triggerCustomEvent('cardStartingThrownIn', this.element, true, this)
      }
    },

    /**
     * When a card is returned to the deck (either manually or when card has not being
     * fully swipped) the throw in animation is triggered. It uses the current card offset
     * position and the sprint plugin to triger the animation of moving the card to the
     * center
     *
     * @param {Number} offsetX last card X swipe offset
     * @param {Number} offsetY last card Y swipe offset
     */
    throwIn(offsetX = null, offsetY = null) {
      this._throw(null, offsetX || this.lastCardOffsetX, offsetY || this.lastCardOffsetY);
    },

    /**
     * When a card is thrown out of the deck the throw out animation is triggered.
     * It uses drag direction and game size to calculate destination coordinates
     *
     * @param {Number} offsetX last card X swipe offset
     * @param {Number} offsetY last card Y swipe offset
     */
    throwOut(offsetX, offsetY) {
      const direction = calculateDirection(
        this.allowedSwipeDirections,
        offsetX,
        offsetY
      );
      this._throw(direction, offsetX, offsetY);
    },

    /**
     * Enables card swipe
     */
    enableSwiping() {
      this.canSwipe = true;
    },

    /**
     * Disables card swipe
     */
    disableSwiping() {
      this.canSwipe = false;
    },

    /**
     * Enables card throw out
     */
    enableThrowing() {
      this.canThrow = true;
    },

    /**
     * Disables card throw out
     */
    disableThrowing() {
      this.canThrow = false;
    },
  });
};
