import anime from 'animejs';

/**
 * Stimulus Controller Mixin for managing animations in a consistent way.
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const animationsHandler = (controller) => {
  Object.assign(controller, {
    newAnimation(targets = [], duration = 0, transformation = {}, easing = '') {
      return Object.assign({ targets, duration, easing }, transformation);
    },

    /**
     * Creates a new animation timeline to trigger animations.
     *
     * @param {Number} duration Default duration for timeline animations
     * @param easing Anime.js Default easing for timeline animations
     * @return {AnimationTimeline} Anime.js timeline that will hold multiple animations
     */
    newAnimationTimeline(duration = 0, easing = '') {
      return anime.timeline({ easing, duration });
    },

    /**
     * Add new animation to timeline
     *
     * @param {AnimationTimeline} timeline
     * @param {Object} animation Anime.js animation specification (target, transform, easing, ...)
     * @param {String || Number} offset Specifies delay depending on previous and initial animation completion
     *
     * @return {AnimationTimeline}
     */
    addAnimationToTimeline(timeline, animation, offset = 0) {
      timeline.add(animation, offset);
      return timeline;
    },
  });
};
