import TestRunModel from '../../../models/screening/test-run-model';
import { Controller } from '@hotwired/stimulus';
import { eventsHandler } from '../../mixins/events-handler';
import { animationsHandler } from '../../mixins/animations-handler';
import { randomizerHandler } from '../../mixins/randomizer-handler';
import { testRunHandler } from '../../mixins/gameplay/test-run-handler';
import {
  addAnimationToTimeline, invertTimeline,
  newAnimationTimeline, playAnimation,
  playTimeline
} from '../../../utils/animations/animation-methods';
import ApiGameSerializer from "../../../serializers/api/api-game-serializer";
import TestRunResultsModel from "../../../models/screening/test-run-results-model";

/**
 * Implements game logic
 */
export default class GameController extends Controller {
  // Game metadata: cards, results, etc
  static values = { gameInformation: Object }

  // Child Controllers
  ui = null
  help = null
  deck = null
  topCard = null
  playedCards = []
  undoneCards = []

  game = (new ApiGameSerializer()).toGameModel(this.gameInformationValue)
  testRun = new TestRunModel()
  testRunResults = {}

  //Timelines
  liftTimeline = null
  helpTimeline = null
  prepareCardTimeline = null
  prepareDeckTimeline = null

  // Status Flags
  rewinding = false
  forwarding = false
  togglingHelp = false
  lastPlayedCardForwarded = false

  showingHelp = false

  initialize() {
    eventsHandler(this)
    testRunHandler(this)
    animationsHandler(this)
    randomizerHandler(this)

    this._registerEventListeners()
  }

  /**
   * Initializes all controller listener events,
   * providing a clean way to have them organized
   *
   * @private
   */
  _registerEventListeners() {
    // Collection of event names and corresponding listener callback method
    const listeners = [
      ['deckReady', (e) => {
        this._registerDeck(e)
        this._notifyGameReady()
      }],
      ['uiInserted', (e) => { this._registerUI(e) }],
      ['helpInserted', (e) => { this._registerHelp(e) }],
      ['cardGrabbed', (e) => this.liftCard()],
      ['cardDropped', (e) => this.dropCard()],
      ['rewindClicked', (e) => this.rewindPlayedCard()],
      ['forwardClicked', (e) => this.undoUndoneCard()],
      ['helpClicked', (e) => this.toggleHelp()],
      ['closeHelpClicked', (e) => this.hideHelp()],
      ['cardDragged', (e) => {
        const movementConfidence = e.detail.confidence
        const movementDirection = e.detail.direction
        this.displaySelectedAnswerInTopCard(movementConfidence, movementDirection)
      }],
      ['cardStartingThrownIn', (e) => this.hideSelectedAnswerInCard(e.detail)],
      ['cardThrownOut', (e) => this.playCardOutOfDeck(e.detail)],
      ['cardThrownIn', (e) => this.returnCardToDeck(e.detail)],
    ]

    listeners.forEach((l) => this.element.addEventListener(l[0], l[1]))
  }

  /**
   * Register UI in controller
   *
   * @param {CustomEvent} uiInsertedEvent
   * @private
   */
  _registerUI(uiInsertedEvent) {
    this.ui = uiInsertedEvent.detail
  }

  /**
   * Register deck in controller
   *
   * @param {CustomEvent} helpInsertedEvent
   * @private
   */
  _registerHelp(helpInsertedEvent) {
    this.help = helpInsertedEvent.detail;
  }

  /**
   * Register deck in controller
   *
   * @param {CustomEvent} deckReadyEvent
   * @private
   */
  _registerDeck(deckReadyEvent) {
    this.deck = deckReadyEvent.detail;
  }

  /**
   * Send custom event to notify the game is ready to be played
   *
   * @private
   */
  _notifyGameReady() {
    this.triggerEvent('gameInserted', this.element.parentElement, true, this)
  }

  /**
   * Send custom event to notify the game is completed
   *
   * @private
   */
  _notifyGameCompleted() {
    this.triggerEvent('gameCompleted', this.element.parentElement, true, this)
  }

  /**
   * Logic to check whether game is completed. Compares number of
   * played cards with total number of cards, indicating the game
   * ends when all cards are played
   *
   * @return {boolean}
   * @private
   */
  _gameCompleted() {
    return this.playedCards.length === this.deck.cards.length
  }

  /**
   * Given a card creates timeline of animation to flip and rotate card
   * in order to be ready for beying played from the top of the deck
   *
   * @param {CardController} card
   * @return {AnimationTimeline}
   * @private
   */
  _prepareCardTimeline(card, rotation = 0) {
    return newAnimationTimeline([card.rotationAnimation(rotation, 500), card.flipAnimation(1200)], '+=200')
  }

  /**
   * Timeline animation that flips top card and slightly rotate remaining ones
   * over their axis in order to set deck ready for players to start the game
   *
   * @param {CardController} topCard
   * @return {AnimationTimeline}
   * @private
   */
  _prepareDeckTimeline(topCard) {
    const remainingCards = this.deck.remainingCards(topCard)

    const tl = newAnimationTimeline(remainingCards
      .map((c, i) => c.rotationAnimation(c.randomRotation(1, 5, i), 500)), 400)
    addAnimationToTimeline(tl, this.topCard.flipAnimation(1200), '+=200')
    if(this.ui) addAnimationToTimeline(tl, this.ui.revealAnimation(400), '+=200')
    return tl;
  }

  /**
   * Timeline animation that sets all deck cards straight,
   * unzooms deck and triggers animation to display help menu
   *
   * @return {AnimationTimeline}
   * @private
   */
  _helpTimeline() {
    if(!this.ui) return newAnimationTimeline()

    let tl = newAnimationTimeline([this.ui.hideAnimation(200)])
    addAnimationToTimeline(tl, this.deck.zoomOutAnimation(0.4, 400), '0')
    tl = this.help.revealTimeline(4, tl)
    return tl
  }

  /**
   * Logic to switch game controller top card. Updates corresponding controller
   * attribute and removes existing card associated timelines in order to be
   * reset
   *
   * @param {CardController} newTopCard new card set at the top of the deck
   * @private
   */
  _switchTopCard(newTopCard) {
    this.topCard = newTopCard
    this.liftTimeline = null
    this.prepareCardTimeline = null
  }

  /**
   * Disables swipe from all deck cards
   *
   * @private
   */
  _disableCardsSwipe() {
    this.deck.cards.forEach((c) => {
      c.disableSwipe()
      c.disableThrowing()
    })
  }

  /**
   * Enables swipe in all deck cards
   *
   * @private
   */
  _enableCardsSwipe() {
    this.deck.cards.forEach((c) => {
      c.enableSwipe()
      c.enableThrowing()
    })
  }

  /**
   * Disable all potential in-game actions. Use it to do not allow
   * user to perform certain operations while specific in-game
   * transitions are running (rewinding, dropping card, etc)
   *
   * @private
   */
  _disableAllGameActions() {
    if(this.ui) {
      this.ui.disableRewind()
      this.ui.disableForward()
      this.ui.disableShowHelp()
    }
    this._disableCardsSwipe()
  }

  /**
   * Consult in-game information and enable all
   * possible game actions according to its status
   *
   * @private
   */
  _enableGameActions() {
    if(this.ui) {
      if(this.playedCards.length > 0) this.ui.enableRewind()
      if(this.undoneCards.length > 0) this.ui.enableForward()
      this.ui.enableShowHelp()
    }

    this._enableCardsSwipe()
  }

  /**
   * Initializes game UI and internal logic
   *
   * @return {Promise<AnimationTimeline>}
   */
  async startGame(_data) {
    this._switchTopCard(this.deck.lastSiblingCard())
    this._disableCardsSwipe()
    this.prepareDeckTimeline = this._prepareDeckTimeline(this.topCard)
    await playTimeline(this.prepareDeckTimeline)
    this._enableCardsSwipe()
    return this.prepareDeckTimeline
  }

  /**
   * Use card zoom effect animation to emulate user
   * is lifting the card when panning the card element
   *
   * @return {AnimationTimeline}
   */
  liftCard() {
    if(!this.liftTimeline) this.liftTimeline = newAnimationTimeline([this.topCard.liftAnimation()])
    return playTimeline(this.liftTimeline, true)
  }

  /**
   * Use reversed card zoom effect animation to
   * emulate user is dropping a lifted card
   *
   * @return {null}
   */
  dropCard() {
    if(!this.liftTimeline) return
    return playTimeline(this.liftTimeline, true)
  }

  /**
   * Logic called when a card is thrown out of deck. Update rewind
   * and forward stacks, enabling rewind/forward if possible; and
   * reveal next top card
   *
   * @param {Card} playedCard
   */
  async playCardOutOfDeck(playedCard) {
    this._disableAllGameActions()

    try {
      playedCard.lastAnswer = playedCard.answer
      this.addPlayedCardToTestRun(this.testRun, this.game, playedCard)
    } catch (e) {
      console.log(e)
    }

    // If game was forwarded mark forward as done and update rewind/forward
    // buttons according to rewind and forward stack statuses
    if(this.forwarding) {
      this.forwarding = false
      if(this.ui) {
        if(this.playedCards.length > 0) this.ui.enableRewind()
        if(this.undoneCards.length > 0) this.ui.enableForward()
      }
      // Whenever a card is played without forwarding the forward stack
      // is emptied and user can not forward until a new rewind is made
    } else {
      if(this.undoneCards.length > 0) {
        this.undoneCards = []
        if(this.ui) this.ui.disableForward()
      }
    }

    this.playedCards.push(playedCard)

    if(this._gameCompleted()) {
      try {
        this.testRunResults = new TestRunResultsModel(this.testRun, this.game)
      } catch(e) { console.log(e) }

      this._notifyGameCompleted()
    } else {
      this._switchTopCard(this.deck.nextCard(playedCard))

      this.prepareCardTimeline = this._prepareCardTimeline(this.topCard)
      await playTimeline(this.prepareCardTimeline, true)

      // Hide card to avoid being seen during help display
      playedCard.hide()

      this._enableGameActions()
    }
  }

  /**
   * Logic called when a card returns to the deck
   *
   * @param card
   */
  returnCardToDeck(card) {
    // Only special logic is needed when a card is returned to deck via rewind
    if(this.rewinding) {
      this.rewinding = false
      this._enableGameActions()
    }
  }

  /**
   * Rewind last played card by hiding current top card
   * and bringing last user played card back to the deck
   */
  async rewindPlayedCard() {
    if(this.playedCards.length === 0) return

    this.rewinding = true
    this._disableAllGameActions()

    // When rewinding multiple times top card is switched and no new
    // card preparation timeline is prepared. This lines secures there
    // is always a prepareCardTimeline available
    if(!this.prepareCardTimeline)
      this.prepareCardTimeline = this._prepareCardTimeline(this.topCard, this.topCard.randomRotation(1, 5, this.playedCards.length))
    await playTimeline(this.prepareCardTimeline, true)

    const undoneCard = this.playedCards.pop()
    this.undoneCards.push(undoneCard)
    this._switchTopCard(undoneCard)

    undoneCard.reveal()
    undoneCard.throwIn()
  }

  /**
   * Logic to undo rewind movements. It indicates the game logic
   * the next card move will be a forwarding and programatically
   * plays last rewinded card in its last swiped position
   */
  undoUndoneCard() {
    if(this.undoneCards.length === 0) return

    this.forwarding = true
    this._disableAllGameActions()

    const rewindedCard = this.undoneCards.pop()
    rewindedCard._throw(rewindedCard.lastDirection, 0, 0)
  }

  /**
   * Request top cards answer prompt to display answer
   * according to card movement and throw confidence
   *
   * @param {Number} confidence 0-1 value indicates user throw completion
   * @param {String} direction direction in which is being dragged
   */
  displaySelectedAnswerInTopCard(confidence = 0, direction = null) {
    const selectedAnswer = this.gameInformationValue.answers.find((a) => a.direction === direction.toLowerCase())
    if(selectedAnswer)
      return this.topCard.setAnswer(selectedAnswer)
  }

  /**
   * Request top card answer prompt to hide answer being displayed
   * over card when returned to deck. If card was rewinded, instead
   * of hiding the answer display the last answer user selected for
   * card
   *
   * @param {CardController} card
   */
  hideSelectedAnswerInCard(card) {
    // Ignore answer unset if card is in rewind process
    if(this.rewinding) return
    return card.unsetAnswer()
  }

  /**
   * Switches help menu display
   *
   * @return {Promise<AnimationTimeline>}
   */
  async toggleHelp() {
    if(this.togglingHelp) return
    this.togglingHelp = true
    this._disableAllGameActions()

    if(!this.helpTimeline) this.helpTimeline = this._helpTimeline()

    await playTimeline(this.helpTimeline, true)

    this.togglingHelp = false
    this._enableGameActions()
    this.showingHelp = !this.showingHelp
  }

  async hideHelp() {
    if(!this.showingHelp) return
    await this.toggleHelp()
  }

  /**
   * Hide game UI once all cards are played and send
   * test run to current scene in order to transfer
   * information to next scene
   *
   * @return {Promise<void>}
   */
  async endGame() {
    if(this.ui) await playAnimation(this.ui.hideAnimation(400))
  }
}
