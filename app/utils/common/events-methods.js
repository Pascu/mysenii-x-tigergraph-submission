/**
 * Trigger a custom event
 *
 * @param {string} eventName Name of event triggered
 * @param {HTMLElement} element HTML element from which event is originated
 * @param {Boolean} bubbles Enable event to bubble up in DOM
 * @param {Object} detail Event payload
 */
function triggerCustomEvent(eventName, element, bubbles = true, detail = null) {
  const newEvent = new CustomEvent(eventName, { bubbles, detail });
  element.dispatchEvent(newEvent);
}

export { triggerCustomEvent };
