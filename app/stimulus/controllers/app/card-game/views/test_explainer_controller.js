import OpacitySplashController from '../../splash/opacity_splash';
import { playTimeline } from '../../../../../utils/animations/animation-methods';
import { graphPlotlyHandler } from '../../../../mixins/graph/graph-plotly-handler';
import PlotlySpiderGraphModel from "../../../../../models/screening/score-graph/plotly/plotly-spider-graph-model";

export default class TestExplainerController extends OpacitySplashController {
  testRunResults = null
  plotlyData = null

  initialize() {
    super.initialize()
    graphPlotlyHandler(this)

    if(!this._canInitialize()) return
  }

  connect() {
    super.connect();

    if(!this._canInitialize()) return
  }

  async showSplash(data = null) {
    this.testRunResults = data
    this.plotlyData = new PlotlySpiderGraphModel([this.testRunResults.testRun.testRunScore])
    this.drawGraph(this.element.querySelector('#test'), this.plotlyData)
    await playTimeline(this.splashTimeline, true)
  }
}
