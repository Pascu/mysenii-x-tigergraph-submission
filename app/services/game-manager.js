import Service from '@ember/service';

const cesdEN = {
  leaflet: {
    pages: [
      {
        type: 'front',
        color: 'purple'
      },
      {
        type: 'introduction',
        color: 'yellow',
        header: 'hey You,<br> how have you<br> been feeling lately?',
        content: 'Mental health is always the priority! Let\'s learn to understand our emotions and how and why we are feeling them.'
      },
      {
        type: 'content',
        color: 'yellow',
        image: 'assets/images/leaflet/image1.webp',
        content: 'Sadness is something we all experience from time to time. For some, this feeling is temporary and goes away on its own. But for others, this feeling stays and becomes more intense, and can become everyday routine.'
      },
      {
        type: 'content',
        color: 'yellow',
        image: 'assets/images/leaflet/image2.webp',
        content: 'If your mood has changed over the last few weeks and engaging in daily task is getting more difficult, then you are not alone. mysenii is with you in this journey, you may start by checking on your feelings with the screening.'
      },
      {
        type: 'back',
        color: 'purple',
        content: 'Play and find out<br>how you feel.<br> Share your results<br>with a professional<br>for more help.',
        footnote: 'The screening tools used by mysenii are scientifically and clinically validated. They are intended to use as initial screening and not as a diagnostic instrument. By tapping Play game,  you are indicating that you have read   and agree to the Terms of Service.'
      },
    ]
  },
  cards: [
    {
      id: '20',
      question: 'It was hard to get started doing things.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/20.png',
      color: 'purple',
      categories: ['Somatic'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '19',
      question: "I felt people didn't like me.",
      clarification: '',
      image: 'assets/images/game/card-illustrations/19.png',
      color: 'orange',
      categories: ['Interpersonal'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '18',
      question: 'I felt sad.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/18.png',
      color: 'yellow',
      categories: ['Depressive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '17',
      question: 'I felt like crying.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/17.png',
      color: 'pink',
      categories: ['Depressive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '16',
      question: 'I had a good time.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/16.png',
      color: 'green',
      categories: ['Positive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '15',
      question:
        "I felt like kids I know were not friendly or that they didn't want to be with me",
      clarification: '',
      image: 'assets/images/game/card-illustrations/15.png',
      color: 'purple',
      categories: ['Interpersonal'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '14',
      question: "I felt lonely, like I didn't have any friends.",
      clarification: '',
      image: 'assets/images/game/card-illustrations/14.png',
      color: 'orange',
      categories: ['Depressive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '13',
      question: 'I was more quiet than usual.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/13.png',
      color: 'yellow',
      categories: ['Somatic'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '12',
      question: 'I was happy.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/12.png',
      color: 'pink',
      categories: ['Positive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '11',
      question: "I didn't sleep as well as I usually sleep.",
      clarification: '',
      image: 'assets/images/game/card-illustrations/11.png',
      color: 'green',
      categories: ['Somatic'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '10',
      question: 'I felt scared.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/10.png',
      color: 'purple',
      categories: ['Depressive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '9',
      question: "I felt like things I did before didn't work out right",
      clarification: '',
      image: 'assets/images/game/card-illustrations/9.png',
      color: 'orange',
      categories: ['Depressive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '8',
      question: 'I felt like something good was going to happen.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/8.png',
      color: 'yellow',
      categories: ['Positive'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '7',
      question: 'I felt like I was too tired to do things.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/7.png',
      color: 'pink',
      categories: ['Somatic'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '6',
      question: 'I felt down and unhappy.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/6.png',
      color: 'green',
      description: 'Card explanation placeholder ...',
      categories: ['Depressive'],
    },
    {
      id: '5',
      question: "I felt like I couldn't pay attention to what I was doing.",
      clarification: '',
      image: 'assets/images/game/card-illustrations/5.png',
      color: 'purple',
      categories: ['Somatic'],
      description: 'Card explanation placeholder ...',
    },
    {
      id: '4',
      question: 'I felt like I was just as good as other kids.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/4.png',
      color: 'orange',
      description: 'Card explanation placeholder ...',
      categories: ['Positive'],
    },
    {
      id: '3',
      question:
        "I wasn't able to feel happy, even when my family or friends tried to help me feel better",
      clarification: '',
      image: 'assets/images/game/card-illustrations/3.png',
      color: 'yellow',
      description: 'Card explanation placeholder ...',
      categories: ['Depressive'],
    },
    {
      id: '1',
      question: "I was bothered by things that usually don't bother me.",
      clarification: '',
      image: 'assets/images/game/card-illustrations/1.png',
      color: 'green',
      description: 'Card explanation placeholder ...',
      categories: ['Somatic'],
    },
    {
      id: '2',
      question: "I did not feel like eating. I wasn't very hungry",
      clarification: '',
      image: 'assets/images/game/card-illustrations/2.png',
      color: 'pink',
      description: 'Depression can affect our appetite and change the relationship that we have with food. It can cause us to eat unhealthily, eat more than usual and it can also lead to a loss of appetite. Depression also causes hormonal effects, by releasing stress hormones, which make our stomach release hunger hormones, like ghrelin which stimulates appetite.',
      categories: ['Somatic'],
    },
  ],
  answers: [
    {
      id: '1',
      title: 'A lot',
      description: 'Five or more days per week',
      direction: 'down',
      lightness: '',
      score: 3
    },
    {
      id: '2',
      title: 'Some',
      description: 'Three to four days per week',
      direction: 'right',
      lightness: '',
      score: 2
    },
    {
      id: '3',
      title: 'A little',
      description: 'One to two days per week',
      direction: 'left',
      lightness: '',
      score: 1
    },
    {
      id: '4',
      title: 'Not at all',
      description: 'Less than a day per week',
      direction: 'up',
      lightness: '',
      score: 0
    },
    {
      id: '6',
      question: 'I felt down and unhappy.',
      clarification: '',
      image: 'assets/images/game/card-illustrations/6.png',
      color: 'green',
      categories: ['Depressive'],
      description: 'Card explanation placeholder ...',
    },
  ],
  tutorial: {
    cards: [
      {
        id: '4',
        question: '<span class="mysenii-h3-bold">This was the tutorial</span>. Now you will see the game cards and will be able to select your answers freely. Enjoy the game!',
        swipe_message: 'swipe Down to finish the tutorial',
        swipe_icon: 'assets/images/ui/white/swipe/down.webp',
        color: 'purple',
        directions: ['DOWN'],
      },
      {
        id: '3',
        question: 'If you want <span class="mysenii-h3-bold">to see all answers at once you can use the information (i) button</span> that will appear at the left-bottom corner of the screen.',
        swipe_message: 'swipe Right to pass to next card',
        swipe_icon: 'assets/images/ui/white/swipe/right.webp',
        color: 'yellow',
        directions: ['RIGHT'],
      },
      {
        id: '2',
        question: 'Each side of the screen (top/bottom/right/left) is an answer. To read each answer <span class="mysenii-h3-bold">swipe slightly the card in each direction while holding it</span>. To select an answer swipe further and drop the card',
        swipe_message: 'swipe Left to pass to next card',
        swipe_icon: 'assets/images/ui/white/swipe/left.webp',
        color: 'orange',
        directions: ['LEFT'],
      },
      {
        id: '1',
        question: 'mysenii is a game of answering questions. Each card, like this one, is <span class="mysenii-h3-bold">a question about how you felt during the last week</span>. To answer a question you swipe the card to any side of the screen',
        directions: ['UP'],
        swipe_message: 'swipe Up to pass to next card',
        header_icon: 'assets/images/ui/white/info.webp',
        swipe_icon: 'assets/images/ui/white/swipe/up.webp',
        color: 'green',
      },
    ]
  },
  results: [
    {
      range: [0,14],
      title: 'You are not experiencing depression symptoms',
      description: 'Depression is complex and it puts us through hard times! Lets understand why you might be feeling this way so we can find the right help for you.'
    },
    {
      range: [15,15],
      title: 'You seem to be experiencing mild depression symptoms',
      description: 'Depression is complex and it puts us through hard times! Lets understand why you might be feeling this way so we can find the right help for you.'
    },
    {
      range: [16,60],
      title: 'You seem to be suffering from severe depressive symptoms',
      description: 'Depression is complex and it puts us through hard times! Lets understand why you might be feeling this way so we can find the right help for you.'
    }
  ]
};

const tests = {
  cesd: {
    'en-us': cesdEN,
  },
};

/**
 * Service responsible of managing the game logic:
 * - Loading decks
 * - Submitting game results
 * - Calculating scores
 * - etc
 */
export default class GameManager extends Service {
  /**
   * Requests a game deck to the server
   * @param {string} deckName - name of the test
   * @param {string} language - language of the test
   * @return {Promise<unknown>}
   */
  loadDeck(deckName = 'cesd', language = 'en-us') {
    return new Promise((resolve) => resolve(tests[deckName][language]));
  }
}
