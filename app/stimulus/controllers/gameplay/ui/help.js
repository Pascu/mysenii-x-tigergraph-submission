import { Controller } from '@hotwired/stimulus';
import { eventsHandler } from '../../../mixins/events-handler';
import {
  addAnimationToTimeline,
  newAnimation,
  newAnimationTimeline,
} from '../../../../utils/animations/animation-methods';

/**
 * Implements ui help logic to display card
 * swipe directions and associated answers
 */
export default class HelpController extends Controller {
  static targets = ['answerArea', 'answerPrompt']

  /**
   * Collection of coordinates used to represent the different regions
   * in which screen will be splited in order to represent the possible
   * swipe directions. Coordinates are grouped by cardinals in order to
   * create different regions depending on the number of available answers
   *
   * @type {{"4": {left: [], up: [], right: [], down: []}}}
   */
  coordinates = {
    4: {
      up: [
        '0 0, 50 0, 100 0, 0 0',
        '0 0, 50 50, 100 0, 0 0'
      ],
      left: [
        '0 0, 0 50, 0 100, 0 0',
        '0 0, 50 50, 0 100, 0 0'
      ],
      down: [
        '0 100, 100 100, 50 100, 0 100',
        '0 100, 100 100, 50 50, 0 100'
      ],
      right: [
        '100 0, 100 50, 100 100, 100 0',
        '100 0, 50 50, 100 100, 100 0'
      ],
    }
  }

  initialize() {
    eventsHandler(this)
  }

  connect() {
    this.triggerEvent('helpInserted', this.element.parentElement, true, this)
  }

  /**
   * Timeline animation that displays help information,
   * showing all possible answer regions user can swipe to
   *
   * @param {Number} answersNumber number of answers. Used to infer proper animation coordinates
   *                               depending on the possible answer regions avaliable in game
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   */
  revealTimeline(answersNumber = 4, timeline = newAnimationTimeline([])) {
    const coordinates = Object.values(this.coordinates[answersNumber])
    const regionAnimations = this.answerAreaTargets.map(
      (p, i) => newAnimation(p, 200, {
        points: coordinates[i].map((cs) => { return { value: cs } }),}, 'linear'))
    const promptAnimations = this.answerPromptTargets.map(
      (a, i) => newAnimation(a, 200, { opacity: { value: 1 } }, 'linear'))
    regionAnimations.forEach((a) => { timeline = addAnimationToTimeline(timeline, a, '+=120') })
    promptAnimations.forEach((a) => { timeline = addAnimationToTimeline(timeline, a, '+=0') })
    return timeline
  }

  /**
   * Send event to notify game controller to hide help menu and return to game
   */
  closeHelp() {
    this.triggerEvent('closeHelpClicked', this.element.parentElement, true, this)
  }
}
