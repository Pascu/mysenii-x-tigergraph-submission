import ApplicationModel from './../application-model';
import {test} from "qunit";

/**
 * Models Game Test Run Results description
 */
export default class TestRunResultsModel extends ApplicationModel{
  /**
   * Test run from which results will be inferred
   * @type {[number, number]}
   */
  testRun;

  /**
   * Game result associated to test run score
   * @type {ResultsModel}
   */
  result;

  /**
   *
   * @param {TestRunModel} testRun
   * @param {GameModel} game
   */
  constructor(testRun, game) {
    super();
    this.testRun = testRun;
    this.result = this._testRunGameResults(testRun, game);
  }

  /**
   *
   * @param {TestRunModel} testRun
   * @param {GameModel} game
   * @return {ResultsModel}
   * @private
   */
  _testRunGameResults(testRun, game) {
    const standardScoreValue = testRun.testRunStandardScoreValue;
    return game.results.find((r) => (r.range[0] <= standardScoreValue && r.range[1] >= standardScoreValue));
  }
}
