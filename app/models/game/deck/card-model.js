import ApplicationModel from './../../application-model';

/**
 * Models a game question answer
 */
export default class CardModel extends ApplicationModel{
  /**
   * Card Question
   * @type {QuestionModel}
   */
  question;

  /**
   * Card Image Path
   * @type {String}
   */
  image;

  /**
   * Card Color
   * @type {String}
   */
  color;

  /**
   * Card position in deck
   * @type {number}
   */
  index;

  constructor(question = '', image = '', color = '', index = 0) {
    super();

    this.image = image;
    this.color = color;
    this.index = index;
    this.question = question;
  }

  /**
   * @return {String}
   */
  get image() {
    return this.image;
  }

  /**
   * @return {String}
   */
  get color() {
    return this.color;
  }

  /**
   * @return {number}
   */
  get index() {
    return this.index;
  }

  /**
   * @return {Question}
   */
  get question() {
    return this.question;
  }

  /**
   * @return {String}
   */
  get questionContent() {
    return this.question.content;
  }

  /**
   * @return {String}
   */
  get questionClarification() {
    return this.question.clarification;
  }

  /**
   *
   * @param {String} answerTitle
   * @return {AnswerModel}
   */
  answerByTitle(answerTitle) {
    return this.question.answerByTitle(answerTitle)
  }
}
