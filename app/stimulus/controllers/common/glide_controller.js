import Glide from '@glidejs/glide';
import { Controller } from '@hotwired/stimulus';
import {exceptionsHandler} from '../../mixins/exceptions-handler';

/**
 * Stimulus contoller for card explainer. Initializes Glider slider
 * with specific configuration and listen to slide events to display
 * card information according to slide
 */
export default class GlideController extends Controller {
  static values = { padding: Number }

  static targets = ['slider']

  slider = null

  initialize() {
    super.initialize()
    exceptionsHandler(this)
  }

  connect() {
    super.connect()
    this._initializeSlider()
  }

  _initializeSlider() {
    if(!this.hasSliderTarget)
      this.throwException('slider', 'Slider')

    this.slider = new Glide(this.sliderTarget, {
      type: 'slider',
      focusAt: 0,
      startAt: 0,
      perView: 1,
      peek: {
        before: 0,
        after: this.hasPaddingValue ? this.paddingValue : 100
      }
    })

    this.slider.mount()
  }
}
