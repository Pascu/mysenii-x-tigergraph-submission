import * as Handlebars from "handlebars";

/**
 * Stimulus Controller Mixin for managing DOM (adding, removing, copying elements).
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const domHandler = (controller) => {
  Object.assign(controller, {
    /**
     * Given a template element returns its inner HTML content
     *
     * @param {HTMLTemplateElement} templateElement
     * @return {string}
     */
    templateHTML(templateElement) {
      const clonedTemplate = templateElement.cloneNode(true)
      const auxiliarContentWrapper = document.createElement('div')
      Array.prototype.slice.call(clonedTemplate.children).forEach((c) => auxiliarContentWrapper.append(c))
      return auxiliarContentWrapper.innerHTML
    },

    /**
     * Updates templateInnerHTML content with data information, returning
     * a new HTML string where variables have been updated with their
     * respective values
     *
     * @param {HTMLTemplateElement} templateElement
     * @param {Object} data contains information referred in template element via handlebars
     * @return {*}
     */
    compiledTemplateHTML(templateElement, data) {
      // Avoids conflicts between Ember and our handlebars template implementation
      // Our handlebars use (()) instead of {{}}
      const precompiledTemplate = this.templateHTML(templateElement)
        .replaceAll('((', '{{').replaceAll('))', '}}')

      const handlebarTemplate = Handlebars.compile(precompiledTemplate)
      return handlebarTemplate(data, {
        // Enables Getter method invokation by Handlebars
        allowProtoPropertiesByDefault: true
      })
    },

    /**
     * Copy template element DOM content inside destination HTML element
     *
     * @param {HTMLTemplateElement} templateElement HTML template whose content will be cloned
     * @param {HTMLElement} destinationElement HTML element where template content will be inserted
     * @param {Boolean} clearDestination if true clear destination element content before inserting template
     * @param {Object} templateData information to be displayed in template element via Handlebars
     */
    insertTemplate(templateElement, destinationElement, clearDestination, templateData = {}) {
      if(clearDestination) destinationElement.innerHTML = ''
      destinationElement.insertAdjacentHTML('beforeend', this.compiledTemplateHTML(templateElement, templateData))
    }
  });
};
