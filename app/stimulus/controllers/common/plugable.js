import { Controller } from '@hotwired/stimulus';

/**
 * Pluggable controllers are inserted and removed from DOM,
 * so they implement a special logic to only initialize and
 * connect when they are not contained in a template
 */
export default class PlugableController extends Controller {
  initialize() {
    super.initialize()
    if(!this._canInitialize()) return
  }

  connect() {
    super.connect()
    if(!this._canInitialize()) return
  }

  /**
   * Pluggable component can be only initialized
   * when element has no parent template element
   *
   * @return {boolean}
   * @private
   */
  _canInitialize() {
    return this.element.closest('template') === null
  }
}
