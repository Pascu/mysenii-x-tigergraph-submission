import { Controller } from '@hotwired/stimulus';
import { eventsHandler } from '../../../mixins/events-handler';
import {
  addAnimationToTimeline,
  newAnimation,
  newAnimationTimeline,
  playAnimation,
  playTimeline
} from '../../../../utils/animations/animation-methods';
import Glide from "@glidejs/glide";
import PlugableController from '../../common/plugable';

/**
 * Implements animation logic to show and hide a splash. This
 * controller can be used for multiple splash screens with
 * similar layout and in/out animations
 */
export default class OpacitySplashController extends PlugableController {
  static targets = ['content', 'leftAsset', 'rightAsset', 'cardSlider']

  cardGlideSlider = null
  splashTimeline = this._revealTimeline()

  initialize() {
    eventsHandler(this)
  }

  connect() {
    this._initializeCardsSlider()
    this.triggerEvent('splashInserted', this.element.parentElement, true, this)
  }

  _initializeCardsSlider() {
    if(!this.hasCardSliderTarget) return

    this.cardGlideSlider = new Glide(this.cardSliderTarget, {
      type: 'slider',
      focusAt: 0,
      startAt: 0,
      perView: 1,
      peek: {
        before: 0,
        after: 200
      }
    })

    this.cardGlideSlider.mount()
  }

  /**
   * Animation to change the opacity of the background, revealing the splash
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _backgroundRevealTimeline(timeline = newAnimationTimeline()) {
    addAnimationToTimeline(timeline,
      newAnimation(this.element, 1200, { backgroundColor: ['#ffffff', '#ffffff'], opacity: [0, 1] }))
    return timeline
  }

  /**
   * Animation to reveal the content via fading
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _contentRevealTimeline(timeline = newAnimationTimeline()) {
    if(this.hasContentTarget)
      addAnimationToTimeline(timeline,
        newAnimation(this.contentTarget, 500, { backgroundColor: ['#ffffff', '#ffffff'], opacity: [0, 1] }), '+=200')
      this.contentTarget
    return timeline
  }

  /**
   * Animation to move images placed in the left side of the
   * screen from outside of the screen to their original position
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _leftAssetsTimeline(timeline = newAnimationTimeline()) {
    if(this.hasLeftAssetTarget)
      this.leftAssetTargets.forEach((t) => {
        addAnimationToTimeline(timeline,
          newAnimation(t, 400, { translateX: [-500, 0] }), '-=300')
      })
    return timeline
  }

  /**
   * Animation to move images placed in the right side of the
   * screen from outside of the screen to their original position
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _rightAssetsTimeline(timeline = newAnimationTimeline()) {
    if(this.hasRightAssetTarget)
      this.rightAssetTargets.forEach((t) => {
        addAnimationToTimeline(timeline,
          newAnimation(t, 400, { translateX: [500, 0] }), '-=100')
      })
    return timeline
  }

  /**
   * Animation to reveal splashscreen. It generates an animation
   * combination of the different timelines of the different splash
   * elements to create a dynamic and reversible introduction
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _revealTimeline(timeline = newAnimationTimeline()) {
    this._backgroundRevealTimeline(timeline)
    this._contentRevealTimeline(timeline)
    return timeline
  }

  /**
   * Reveal splash screen
   *
   * @return {Promise<void>}
   */
  async showSplash(data = null) {
    this.splashData = data
    await playTimeline(this.splashTimeline, true)
  }

  /**
   * Hide splash screen
   *
   * @return {Promise<void>}
   */
  async hideSplash() {
    await playTimeline(this.splashTimeline, true)
  }

  /**
   * Notify parent component user selected button to close splash
   */
  closeSplash() {
    this.triggerEvent('closeSplash', this.element.parentElement, true, this)
  }
}
