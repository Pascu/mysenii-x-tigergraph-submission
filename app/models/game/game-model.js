import ApplicationModel from './../application-model';
import ResultsModel from './results-model';

/**
 * Models a Card Game
 */
export default class GameModel extends ApplicationModel{
  /**
   * Game Deck
   * @type {DeckModel}
   */
  deck;

  /**
   * Collection of all possible game results
   * @type {ResultsModel[]}
   */
  results;

  constructor(deck = [], results = new ResultsModel()) {
    super();
    this.deck = deck;
    this.results = results;
  }

  /**
   * @return {DeckModel}
   */
  get deck() {
    return this.deck;
  }

  /**
   * @type {ResultsModel[]}
   */
  get results() {
    return this.results;
  }

  /**
   *
   * @param {String} questionContent
   * @return {CardModel}
   */
  cardByQuestionContent(questionContent) {
    return this.deck.cardByQuestionContent(questionContent)
  }

  /**
   *
   * @param {String} questionContent
   * @param {String} answerTitle
   * @return {AnswerModel}
   */
  answerByQuestionContentAndAnswerTitle(questionContent, answerTitle) {
    const card = this.cardByQuestionContent(questionContent)
    return card.answerByTitle(answerTitle)
  }
}
