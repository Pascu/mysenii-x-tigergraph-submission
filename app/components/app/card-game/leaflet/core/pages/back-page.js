import Component from '@glimmer/component';

export default class BackPage extends Component {
  get page() {
    return this.args.page;
  }

  get pageNumber() {
    return (4 - this.args.index)
  }
}
