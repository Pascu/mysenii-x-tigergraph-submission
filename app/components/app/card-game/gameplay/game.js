import Component from '@glimmer/component';

export default class Game extends Component {
  get stringifiedGame() {
    return JSON.stringify(this.args.deck);
  }

  get cards() {
    return this.args.deck.cards;
  }
}
