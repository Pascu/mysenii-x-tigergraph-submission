import { Controller } from '@hotwired/stimulus';
import {eventsHandler} from "../../../../mixins/events-handler";

/**
 * Application is structured in scenes. Each scene contains a view.
 * The view defines the content of each scene, and coordinates all
 * element with common methods to initialize, show and hide the
 * content of the scene
 */
export default class ViewController extends Controller {
  // Default timeline all view controllers use to
  // manage the animations to show and hide the views
  viewTimeline = null

  initialize() {
    super.initialize()
    eventsHandler(this)
  }

  connect() {
    super.initialize()
    this.triggerViewReadyEvent()
  }

  /**
   * Sends a notification to parent component to notify the view is
   * ready to be displayed. This is designed to simplifying the
   * connection of the view component with the scene component
   */
  triggerViewReadyEvent() {
    this.triggerEvent('viewReady', this.element.parentElement, true, this)
  }

  async initializeView() {
  }

  async showView() {
  }

  async hideView() {
  }

  closeView() {
    this.triggerEvent('closeLeaflet', this.element.parentElement, true, this)
  }
}
