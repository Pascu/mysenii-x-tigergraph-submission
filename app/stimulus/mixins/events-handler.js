import { triggerCustomEvent } from '../../utils/common/events-methods';

/**
 * Stimulus Controller Mixin for triggering custom events from DOM elements via Javascript.
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const eventsHandler = (controller) => {
  Object.assign(controller, {
    /**
     * Trig
     * @param {string} event Name of event triggered
     * @param {HTMLElement} element HTML element from which event is originated
     * @param {Boolean} bubbles Enable event to bubble up in DOM
     * @param {Object} detail Event payload
     */
    triggerEvent(event, element, bubbles = true, detail = null) {
      triggerCustomEvent(event, element, bubbles, detail);
    },
  });
};
