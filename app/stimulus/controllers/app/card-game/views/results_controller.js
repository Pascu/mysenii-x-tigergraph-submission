import OpacitySplashController from '../../splash/opacity_splash';
import {playTimeline} from '../../../../../utils/animations/animation-methods';

export default class ResultsController extends OpacitySplashController {
  async showSplash(data = null) {
    this.splashData = data
    await playTimeline(this.splashTimeline, true)
  }
}
