import raf from 'raf';
import { cardHammerConfiguration, flipPage } from '../../../utils/gameplay/drag-methods';
import { triggerCustomEvent } from '../../../utils/common/events-methods';
import { isTouchDevice } from '../../../utils/common/device-methods';
import { cardSpringConfiguration, springPage } from "../../../utils/gameplay/spring-methods";

/**
 * Stimulus Controller Mixin for implementing game swipe mechanics
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const flipHandler = (controller) => {
  Object.assign(controller, {
    /**
     * Determines if given certain page flip the flipped page will
     * return its original position or fully flip once freed by user
     *
     * @param {HTMLElement} pageElement page element being rotated
     * @param {Number} flipThreshold rotation angle after which page is considered to be flipped
     * @return {boolean}
     * @private
     */
    _canFlip(rotation, flipThreshold = 10) {
      return this.swipingLeft ?
        Math.abs(this.lastRotation) > flipThreshold : Math.abs(this.lastRotation) < (180 - flipThreshold)
    },

    _hammerConfiguration() {
      return cardHammerConfiguration(
        // Swipe events are registered by leaflet controller, not pages
        this.element,
        1,
        (event) => {
          this.swiping = true
          this.swipingLeft = event.deltaX < 0
          this.currentRotation = this.lastRotation

          const controller = this

          const swipeEvent = this.swipingLeft ? 'pageStartLeftSwipe' : 'pageStartRightSwipe'
          triggerCustomEvent(swipeEvent, this.element, true, this);

          // raf creates an animation loop which calls
          // the animation method until animation is over
          (function animation() {
            if (controller.swiping && controller.canSwipe && controller.topPage) {
              flipPage(
                controller.topPage.element.querySelector('.mysenii-page-flipper'),
                controller.lastRotation,
                controller.currentRotation
              );
              raf(animation);
            }
          })();
        },
        (event) => {
          // Swiping left locks movement between -180 & 0 for page flip
          const newRotation = this.lastRotation + event.deltaX
          if(newRotation >= -180 && newRotation <= 0) this.currentRotation = newRotation
        },
        (event) => {
          this.swiping = false;
          this.lastRotation = this.currentRotation
          this._throw(this.lastRotation)
          triggerCustomEvent('pageDropped', this.element, true, this)
        },
        () => this.canSwipe
      )
    },

    _springConfiguration() {
      return cardSpringConfiguration(
        100,
        20,
        500,
        150,
        (spring) => {
          const endRotation = this.swipingLeft ? 0 : -180
          this.lastRotation = springPage(
            spring,
            this.topPage.element.querySelector('.mysenii-page-flipper'),
            this.lastRotation,
            endRotation
          )
        },
        (spring) => {
          const endRotation = this.swipingLeft ? -180 : 0
          this.lastRotation = springPage(
            spring,
            this.topPage.element.querySelector('.mysenii-page-flipper'),
            this.lastRotation,
            endRotation
          )
        },
        (spring) => {
          this.canSwipe = true;
          const event = this.swipingLeft ? 'currentPageFlippedBack' : 'previousPageFlippedBack'
          triggerCustomEvent(event, this.element, true, this);
        },
        (spring) => {
          this.canSwipe = true;
          const event = this.swipingLeft ? 'currentPageFlippedForward' : 'previousPageFlippedForward'
          triggerCustomEvent(event, this.element, true, this);
        }
      )
    },

    _initializeFlip() {
      // Swipe status
      this.swiping = false;
      this.canSwipe = true;

      // Tracks initial swap direction. It helps to identify if flipped
      // page is the current page (left swipe) or the previous one (right swipe)
      this.swipingLeft = false

      // Swipe movement track
      this.currentRotation = 0;
      this.lastRotation = 0;

      // Hammer JS drag initializer
      this.hammerInstance = this._hammerConfiguration();

      // In/Out Spring initializer
      const sc = this._springConfiguration();
      this.throwInSpring = sc[0];
      this.throwOutSpring = sc[1];

      // Touch Device Logic
      if (isTouchDevice()) {
        // Even triggers for start & stop touching the screen
        this.element.addEventListener('touchstart', (event) => {
          triggerCustomEvent('panstart', this.element, true, event);
        });

        this.element.addEventListener('touchend', (event) => {
          triggerCustomEvent('panend', this.element, true, event);
        });
        // Non-Touch Device Logic
      } else {
        // Even triggers for start & stop dragging the mouse while clicked
        this.element.addEventListener('mousedown', (event) => {
          triggerCustomEvent('panstart', this.element, true, event);
        });

        this.element.addEventListener('mouseup', (event) => {
          if (this.swiping && !this.panning)
            triggerCustomEvent('panend', this.element, true, event);
        });
      }
    },

    /**
     * Generic throw logic to use the spring system to move a card
     * between a destination and an origin. Implements two logics for
     * throwing cards out of the deck and in the deck.
     *
     * @param direction
     * @param originX
     * @param originY
     * @param direction
     * @private
     */
    _throw(rotation) {
      this.canSwipe = false;
      this.lastRotation = rotation;

      if(this._canFlip(rotation)) {
        this.throwOutSpring
          .setCurrentValue(0)
          .setAtRest()
          .setVelocity(100)
          .setEndValue(1);
        triggerCustomEvent('pageStartingFlippingForward', this.element, true, this)
      } else {
        this.throwInSpring.setCurrentValue(0).setAtRest().setEndValue(1);
        triggerCustomEvent('pageStartingFlippingBack', this.element, true, this)
      }
    },
  })
};
