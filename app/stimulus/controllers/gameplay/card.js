import { Controller } from '@hotwired/stimulus';
import { swipeHandler } from '../../mixins/swipe-handler';
import { eventsHandler } from '../../mixins/events-handler';
import { newAnimation } from '../../../utils/animations/animation-methods';
import anime from 'animejs';

/**
 * Implements card logic (flip, rotate and dragging mechanics)
 */
export default class CardController extends Controller {
  // SwipeElement: DOM element configured with Hammer swipe logic
  static targets = ['innerCard', 'swipeElement'];
  static values = { model: Object }

  // Answer Prompt Controller
  answerPrompt = null

  // User card's selected answer
  answer = null
  lastAnswer = null

  initialize() {
    swipeHandler(this)
    eventsHandler(this)

    this._registerEventListeners()

    // Initialize swipe card logic
    // TODO: better rename this mixin
    this._initializeCard()
  }

  connect() {
    this.triggerEvent('cardInserted', this.element.parentElement, true, this)
  }

  /**
   * Initializes all controller listener events,
   * providing a clean way to have them organized
   *
   * @private
   */
  _registerEventListeners() {
    // Collection of event names and corresponding listener callback method
    const listeners = [
      ['propmtInserted', (e) => this._registerAnswerPrompt(e)]
    ]

    listeners.forEach((l) => this.element.addEventListener(l[0], l[1]))
  }

  /**
   * Registers Answer Prompt in card
   *
   * @param {Event} answerPromptInsertedEvent
   * @private
   */
  _registerAnswerPrompt(answerPromptInsertedEvent) {
    this.answerPrompt = answerPromptInsertedEvent.detail
  }

  /**
   * Card animation, 3D rotation over Y axis to reveal back and front of card
   *
   * @param duration
   * @return {{duration: number, complete: null, targets: *[], easing: string, begin: null}}
   */
  flipAnimation(duration = 2500) {
    return newAnimation(
      this.innerCardTarget,
      duration,
      { rotateY: '+=180' },
      'easeOutElastic(.2, 1)')
  }

  /**
   * Card animation, rotation over Z axis
   *
   * @param {Number} angle degrees of rotation
   * @param {Number} duration
   * @return {{duration: *, targets: *, easing: *}}
   */
  rotationAnimation(angle = this.randomRotation(), duration = 2000) {
    return newAnimation(
      this.innerCardTarget,
      duration,
      { rotateZ: { value: angle } },
      'easeOutElastic(.2, 1)')
  }

  /**
   * Card animation, zooms card to create an effect of the card being lifted
   *
   * @param {Number} duration
   * @return {{duration: number, complete: null, targets: *[], easing: string, begin: null}}
   */
  liftAnimation(duration = 500) {
    return newAnimation(
      this.innerCardTarget,
      duration,
      { scale: { value: 1.05 } },
      'easeOutElastic(.2, 1)')
  }

  /**
   * Generates a random angle that can be used for the rotation animation
   *
   * @param {Number} minAngle
   * @param {Number} maxAngle
   * @param {Number} direction if odd rotate left, if pair rotate right
   * @return {number}
   */
  randomRotation(minAngle = 1, maxAngle = 5, direction = 1) {
    return anime.random(minAngle, maxAngle) * (direction % 2 ? -1 : 1)
  }

  /**
   * Disable card swipe by removing all pointer events from card element
   */
  disableSwipe() {
    this.canSwipe = false
  }

  /**
   * Enable card swipe by restoring all pointer events in card element
   */
  enableSwipe() {
    this.canSwipe = true
  }

  /**
   * Hides card in UI
   */
  hide() {
    this.element.style.opacity = 0
  }

  /**
   * Reveals card in UI
   */
  reveal() {
    this.element.style.opacity = 1
  }

  /**
   * Updates card with selected answer and requests
   * prompt to display answer information
   *
   * @param {Object} answer
   * @return {*}
   */
  setAnswer(answer) {
    if(this.answer === answer) return
    this.answer = answer

    return this.answerPrompt.update(
      answer.title,
      answer.description,
      `assets/images/ui/swipe/${answer.direction}.webp`,
      0)
  }

  /**
   * Updates card by removing last selected answer
   * from prompt. If the card has been rewinded it
   * does not removes the answer, but displays the
   * rewinded answer instead
   *
   * @return {void|Promise<void>|*}
   */
  unsetAnswer() {
    // Thrown cards have last direction assigned. For this case
    // the prompt just needs to be updated with last answer
    if(this.lastAnswer) {
      return this.setAnswer(this.lastAnswer)
    } else {
      return this.answerPrompt.hide()
    }
  }
}
