import Component from '@glimmer/component';

export default class FrontPage extends Component {
  get page() {
    return this.args.page;
  }
}
