import { Controller } from '@hotwired/stimulus';
import { eventsHandler } from '../../../mixins/events-handler';
import {
  newAnimation,
  newAnimationTimeline,
  playAnimation,
  playTimeline
} from '../../../../utils/animations/animation-methods';

/**
 * Implements answer prompt in card logic (display answer selected based
 * on card movement)
 */
export default class AnswerPromptController extends Controller {
  static targets = ['prompt', 'answerTitle', 'answerDescription', 'answerIcon'];

  lastAnswer = null
  // Animation to reveal/hide entire prompt
  promptTimeline = null
  // Animation to toggle selected answer smoothly
  promptTitleTimeline = null

  initialize() {
    eventsHandler(this)
  }

  connect() {
    this.triggerEvent('propmtInserted', this.element.parentElement, true, this)
  }

  _refreshPrompt() {
    this.answerIconTarget.src = this.lastAnswerIcon
    this.answerTitleTarget.innerText = this.lastAnswer
    this.answerDescriptionTarget.innerText = this.lastAnswerDescription
  }

  /**
   * Prompt animation timeline to reveal prompt background
   *
   * @return {AnimationTimeline}
   * @private
   */
  _revealTimeline() {
    return newAnimationTimeline([
      newAnimation(this.element, 200, { opacity: 1 }, 'linear'),
    ], 0)
  }

  /**
   * Prompt animation timeline to reveal prompt content
   * (once background is already visible)
   *
   * @param {Function} intermediateCallback
   * @return {AnimationTimeline}
   * @private
   */
  _toggleAnswerTimeline(intermediateCallback) {
    return newAnimationTimeline([
      newAnimation(this.promptTargets, 200, { opacity: 0 }, 'linear', null,
        () => { intermediateCallback() }),
      newAnimation(this.promptTargets, 200, { opacity: 1 }, 'linear')
    ], '+=0')
  }

  /**
   * Hides prompt
   */
  async hide() {
    this.lastAnswer = null
    await playTimeline(this.promptTitleTimeline, true)
    await playTimeline(this.promptTimeline, true)
  }

  /**
   * Update prompt information with given data and
   * runs animation to smoothly replace prompt content
   *
   * @param {String} answerTitle
   * @param {String} answerDescription
   * @param {String} answerIcon
   * @param {Number} answerConfidence
   */
  async update(answerTitle, answerDescription, answerIcon, answerConfidence) {
    // if answer did not change during movements do not trigger any renders
    if(this.lastAnswer === answerTitle) return

    const firstUpdate = (this.lastAnswer === null)

    this.lastAnswer = answerTitle
    this.lastAnswerIcon = answerIcon
    this.lastAnswerDescription = answerDescription

    if(!this.promptTimeline) this.promptTimeline = this._revealTimeline()
    if(!this.promptTitleTimeline) this.promptTitleTimeline =
      this._toggleAnswerTimeline(() => this._refreshPrompt())

    // card is lifted and user swipes to fist direction
    if(firstUpdate) {
      this._refreshPrompt()
      playTimeline(this.promptTimeline, true)
      playTimeline(this.promptTitleTimeline, true)
    // user swipes between directions without dropping the card
    } else {
      await playTimeline(this.promptTitleTimeline, true)
      await playTimeline(this.promptTitleTimeline, true)
    }
  }
}
