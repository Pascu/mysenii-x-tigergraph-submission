import PlugableController from '../../../common/plugable';
import { eventsHandler } from '../../../../mixins/events-handler';
import {
  newAnimation,
  newAnimationTimeline,
  addAnimationToTimeline,
  playTimeline
} from "../../../../../utils/animations/animation-methods";

/**
 * Implements logic to handle leaflet logic
 */
export default class PageController extends PlugableController {
  static targets = ['foldedCorner']

  initialize() {
    super.initialize()
    eventsHandler(this)
  }

  connect() {
    super.initialize()
    this.triggerEvent('pageInserted', this.element.parentElement, true, this)
  }

  foldAnimation(duration = 1000) {
    return newAnimation(
      this.hasFoldedCornerTarget ? this.foldedCornerTarget : [],
      duration,
      {
        'border-top-width': 30,
        'border-right-width': 30
      },
      'easeOutElastic(.2, 1)')
  }

  foldTimeline(timeline = newAnimationTimeline([])) {
    addAnimationToTimeline(timeline, this.foldAnimation())
    return timeline
  }

  async foldCorner() {
    await playTimeline(this.foldTimeline())
  }
}
