import { Controller } from '@hotwired/stimulus';
import {newAnimation, playAnimation} from '../../../utils/animations/animation-methods';
import anime from 'animejs';

/**
 * Implements logic to flip DOM elements
 * designed with a flippable card layout
 */
export default class CardFlipController extends Controller {
  static targets = ['innerCard'];

  /**
   * Card animation, 3D rotation over Y axis to reveal back and front of card
   *
   * @param duration
   * @return {{duration: number, complete: null, targets: *[], easing: string, begin: null}}
   */
  flipAnimation(duration = 2500) {
    return newAnimation(
      this.innerCardTarget,
      duration,
      { rotateY: '+=180' },
      'easeOutElastic(.2, 1)')
  }

  /**
   * Action to trigger card flip animation
   *
   * @return {*}
   */
  flip() {
    return playAnimation(this.flipAnimation())
  }
}
