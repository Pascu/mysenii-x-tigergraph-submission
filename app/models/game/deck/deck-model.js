import ApplicationModel from './../../application-model';

/**
 * Models a deck
 */
export default class DeckModel extends ApplicationModel{
  /**
   * Deck Cards
   * @type {Array[CardModel]}
   */
  cards;

  constructor(cards = []) {
    super();

    this.cards = cards;
  }

  /**
   * @return {Array[CardModel]}
   */
  get cards() {
    return this.cards;
  }

  /**
   *
   * @param {QuestionModel} questionContent
   * @return {CardModel}
   */
  cardByQuestionContent(questionContent) {
    return this.cards.find((c) => c.questionContent === questionContent)
  }
}
