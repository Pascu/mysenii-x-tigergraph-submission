/**
 * Stimulus Controller Mixin for managing plotly based graphs.
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const graphPlotlyHandler = (controller) => {
  Object.assign(controller, {
    /**
     * Draw Plotly graph
     * @param {HTMLElement} target
     * @param {plotlyGraphModel} plotlyGraphModel
     */
    drawGraph(target, plotlyGraphModel) {
      Plotly.newPlot(target, plotlyGraphModel.data, plotlyGraphModel.layout, { responsive: true });
    }
  });
};
