import { Controller } from '@hotwired/stimulus';
import { eventsHandler } from '../../../mixins/events-handler';
import {
  newAnimation,
  newAnimationTimeline,
  playTimeline,
  playAnimation
} from '../../../../utils/animations/animation-methods';

/**
 * Implements ui logic
 */
export default class UIController extends Controller {
  static targets = ['rewindButton', 'forwardButton', 'showHelpButton']

  canRewind = true
  canForward = true
  canShowHelp = true
  uiDisabled = false
  uiTimeline = null
  rewindButtonTimeline = newAnimationTimeline([this._opacityAnimation(this.rewindButtonTarget, 0.4, 200)])
  forwardButtonTimeline = newAnimationTimeline([this._opacityAnimation(this.forwardButtonTarget, 0.4, 200)])
  showHelpButtonTimeline = newAnimationTimeline([this._opacityAnimation(this.showHelpButtonTarget, 0.4, 200)])

  initialize() {
    eventsHandler(this)

    playAnimation(this.hideAnimation(0))
  }

  connect() {
    this.triggerEvent('uiInserted', this.element.parentElement, true, this)
  }

  /**
   * Enable/Disable all UI actions by switching their respective
   * enable flag values to true (enabled) or false (disabled)
   *
   * @param {Boolean} enable enable flag value
   * @private
   */
  _toggleAllActions(enable) {
    this.canRewind = enable
    this.canForward = enable
    this.canShowHelp = enable
  }

  /**
   * Generic opacity animation to abstract the multiple component opacity animations
   *
   * @param element
   * @param opacity
   * @param duration
   * @return {{duration: number, complete: null, targets: *[], easing: string, begin: null}}
   * @private
   */
  _opacityAnimation(element, opacity, duration) {
    return newAnimation(element, duration, { opacity }, 'easeInQuad')
  }

  /**
   * UI animation, hides revealed UI
   *
   * @param duration
   * @return {{duration: number, complete: null, targets: *[], easing: string, begin: null}}
   */
  hideAnimation(duration) {
    return this._opacityAnimation(this.element, 0, duration)
  }

  /**
   * UI animation, reveals hidden UI
   *
   * @param duration
   * @return {{duration: number, complete: null, targets: *[], easing: string, begin: null}}
   */
  revealAnimation(duration) {
    return this._opacityAnimation(this.element, 1, duration)
  }

  /**
   * UI button options can be disabled and enabled via a boolean flag
   * and the same opacity animation. This method abstract both enable
   * and disable mechanics for all UI buttons
   *
   * @param {Boolean} enabled if true option is enabled, otherwise it is disabled
   * @param {String} optionFlag name of UI property to enable/disable button
   * @param {AnimationTimeline} optionTimeline animation timeline corresponding to UI button
   * @return {AnimationTimeline}
   * @private
   */
  async _toggleUiOption(enabled, optionFlag, optionTimeline) {
    this[optionFlag] = enabled
    await playTimeline(optionTimeline, true)
    return optionTimeline
  }

  /**
   * Enable rewind button
   */
  async enableRewind() {
    if(this.canRewind) return
    await this._toggleUiOption(true, 'canRewind', this.rewindButtonTimeline)
  }

  /**
   * Disable rewind button
   */
  async disableRewind() {
    if(!this.canRewind) return
    await this._toggleUiOption(false, 'canRewind', this.rewindButtonTimeline)
  }

  /**
   * Notify game component to trigger rewind logic
   */
  rewind() {
    if(!this.canRewind) return
    this.triggerEvent('rewindClicked', this.element.parentElement, true, this)
  }

  /**
   * Enable rewind button
   */
  async enableForward() {
    if(this.canForward) return
    await this._toggleUiOption(true, 'canForward', this.forwardButtonTimeline)
  }

  /**
   * Disable rewind button
   */
  async disableForward() {
    if(!this.canForward) return
    await this._toggleUiOption(false, 'canForward', this.forwardButtonTimeline)
  }

  /**
   * Notify game component to trigger rewind logic
   */
  forward() {
    if(!this.canForward) return
    this.triggerEvent('forwardClicked', this.element.parentElement, true, this)
  }

  /**
   * Enable show help button
   */
  enableShowHelp() {
    if(this.canShowHelp) return
    this._toggleUiOption(true, 'canShowHelp', this.showHelpButtonTimeline)
  }

  /**
   * Disable show help button
   */
  disableShowHelp() {
    if(!this.canShowHelp) return
    this._toggleUiOption(false, 'canShowHelp', this.showHelpButtonTimeline)
  }

  /**
   * Notify game component to trigger help display logic
   */
  showHelp() {
    if(!this.canShowHelp) return
    this.triggerEvent('helpClicked', this.element.parentElement, true, this)
  }
}
