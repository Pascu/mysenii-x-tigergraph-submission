import rebound from 'rebound';
import {
  defaultMovementTransformation, defaultPageFlipTransformation,
} from './drag-methods';

/**
 * Once spring animation is triggered it uses a callback to update the animation per frame.
 * This method reads the spring animation value on each callback call to infer the position
 * of the animated element according to it, moving it between springOrigin & springEnd via
 * default card animation
 *
 * It returns the current calculated position of the element to allow other game logics to
 * keep track of the status of the element
 *
 * @param springAnimationProgress
 * @param springElement
 * @param springOriginX
 * @param springOriginY
 * @param springEndX
 * @param springEndY
 * @return {number[]}
 */
function springCard(
  springAnimationProgress,
  springElement,
  springOriginX,
  springOriginY,
  springEndX,
  springEndY
) {
  // Spring animation progress (from 0, begining to 1, end)
  const value = springAnimationProgress.getCurrentValue();

  // Normalize distance from card position to origin. Then with current
  // value infers current progress of spring animations, continuing the loop
  const offsetX = rebound.MathUtil.mapValueInRange(
    value,
    0,
    1,
    springOriginX,
    springEndX
  );
  const offsetY = rebound.MathUtil.mapValueInRange(
    value,
    0,
    1,
    springOriginY,
    springEndY
  );

  defaultMovementTransformation(springElement, offsetX, offsetY);

  return [offsetX, offsetY];
}

/**
 *
 * @param springAnimationProgress
 * @param springElement
 * @param springOrigin
 * @param springEnd
 * @return {*[]}
 */
function springPage(
  springAnimationProgress,
  springElement,
  springOrigin,
  springEnd
) {
  // Spring animation progress (from 0, begining to 1, end)
  const value = springAnimationProgress.getCurrentValue();

  // Normalize distance from card position to origin. Then with current
  // value infers current progress of spring animations, continuing the loop
  const rotation = rebound.MathUtil.mapValueInRange(
    value,
    0,
    1,
    springOrigin,
    springEnd
  );
  defaultPageFlipTransformation(springElement, rotation);

  return rotation;
}

/**
 * Set throwIn and throwOut springers for card drop
 *
 * @param throwInTension
 * @param throwInFriction
 * @param throwOutTension
 * @param throwOutFriction
 * @param onSpringThrowIn
 * @param onSpringThrowOut
 * @param onSpringThrowInAtRest
 * @param onSpringThrowOutAtRest
 * @return {Spring[]}
 */
function cardSpringConfiguration(
  throwInTension,
  throwInFriction,
  throwOutTension,
  throwOutFriction,
  onSpringThrowIn,
  onSpringThrowOut,
  onSpringThrowInAtRest,
  onSpringThrowOutAtRest
) {
  const springer = new rebound.SpringSystem();
  const throwInSpring = springer.createSpring(throwInTension, throwInFriction);
  const throwOutSpring = springer.createSpring(
    throwOutTension,
    throwOutFriction
  );

  throwInSpring.setRestSpeedThreshold(0.05);
  throwOutSpring.setRestSpeedThreshold(0.05);
  throwInSpring.setRestDisplacementThreshold(0.05);
  throwOutSpring.setRestDisplacementThreshold(0.05);

  throwInSpring.addListener({
    onSpringAtRest(spring) {
      onSpringThrowInAtRest(spring);
    },
    onSpringUpdate: (spring) => {
      onSpringThrowIn(spring);
    },
  });
  throwOutSpring.addListener({
    onSpringAtRest(spring) {
      onSpringThrowOutAtRest(spring);
    },
    onSpringUpdate: (spring) => {
      onSpringThrowOut(spring);
    },
  });

  return [throwInSpring, throwOutSpring];
}

export { springCard, springPage, cardSpringConfiguration };
