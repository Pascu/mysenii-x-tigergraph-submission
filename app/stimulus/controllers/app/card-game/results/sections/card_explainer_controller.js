import Glide from '@glidejs/glide';
import { Controller } from '@hotwired/stimulus';
import { exceptionsHandler } from '../../../../../mixins/exceptions-handler';

/**
 * Stimulus contoller for card explainer. Initializes Glider slider
 * with specific configuration and listen to slide events to display
 * card information according to slide
 */
export default class CardExplainerController extends Controller {
  static targets = ['cardSlider', 'descriptionSlider']

  cardGlideSlider = null
  descriptionGlideSlider = null

  initialize() {
    super.initialize()
    exceptionsHandler(this)
  }

  connect() {
    super.connect()
    this._initializeCardsSlider()
    this._initializeDescriptionsSlide()
  }

  _initializeCardsSlider() {
    if(!this.hasCardSliderTarget)
      this.throwException('cardExplainer', 'Card explainer has no card slider target')

    this.cardGlideSlider = new Glide(this.cardSliderTarget, {
      type: 'slider',
      focusAt: 0,
      startAt: 0,
      perView: 1,
      peek: {
        before: 0,
        after: 180
      }
    })

    this.cardGlideSlider.on('swipe.end', (move) => {
      if(this.cardGlideSlider !== this.descriptionGlideSlider)
        this.descriptionGlideSlider.go(`=${this.cardGlideSlider.index}`)
    })

    this.cardGlideSlider.mount()
  }

  _initializeDescriptionsSlide() {
    if(!this.hasDescriptionSliderTarget)
      this.throwException('cardExplainer', 'Card explainer has no slider target')

    this.descriptionGlideSlider = new Glide(this.descriptionSliderTarget, {
      type: 'slider',
      focusAt: 0,
      startAt: 0,
      perView: 1
    })

    this.descriptionGlideSlider.on('swipe.end', (move) => {
      if(this.cardGlideSlider !== this.descriptionGlideSlider)
        this.cardGlideSlider.go(`=${this.descriptionGlideSlider.index}`)
    })

    this.descriptionGlideSlider.mount()
  }
}
