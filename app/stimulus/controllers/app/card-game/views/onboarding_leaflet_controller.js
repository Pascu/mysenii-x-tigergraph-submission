import ViewController from './view_controller';
import {
  addAnimationToTimeline,
  newAnimationTimeline,
  newAnimation, playTimeline,
} from '../../../../../utils/animations/animation-methods';

export default class OnboardingLeafletController extends ViewController {
  static targets = ['leftAsset', 'rightAsset']

  leafletController = null
  viewTimeline = this._revealTimeline()

  initialize() {
    super.initialize()
    this._registerEventListeners()
  }

  connect() {
    super.initialize()
    this.triggerViewReadyEvent()
  }

  /**
   * Initializes all controller listener events,
   * providing a clean way to have them organized
   *
   * @private
   */
  _registerEventListeners() {
    // Collection of event names and corresponding listener callback method
    const listeners = [
      ['leafletReady', (e) => this._registerLeaflet(e)],
    ]

    listeners.forEach((l) => this.element.addEventListener(l[0], l[1]))
  }

  /**
   * Registers page controller in leaflet. When all pages are
   * registered top page is assigned to leaflet to start scroll
   * and leaflet ready notification is sent
   *
   * @param {CustomEvent} leafletInsertedEvent
   * @private
   */
  _registerLeaflet(leafletInsertedEvent) {
    this.leafletController = leafletInsertedEvent.detail
  }

  /**
   * Animation to move images placed in the left side of the
   * screen from outside of the screen to their original position
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _leftAssetsTimeline(timeline = newAnimationTimeline()) {
    if(this.hasLeftAssetTarget)
      this.leftAssetTargets.forEach((t) => {
        addAnimationToTimeline(timeline,
          newAnimation(t, 400, { translateX: [-500, 0] }), '-=100')
      })
    return timeline
  }

  /**
   * Animation to move images placed in the right side of the
   * screen from outside of the screen to their original position
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _rightAssetsTimeline(timeline = newAnimationTimeline()) {
    if(this.hasRightAssetTarget)
      this.rightAssetTargets.forEach((t) => {
        addAnimationToTimeline(timeline,
          newAnimation(t, 400, { translateX: [500, 0] }), '-=100')
      })
    return timeline
  }

  /**
   * Animation to move images placed in the right side of the
   * screen from outside of the screen to their original position
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _leafletTimeline(timeline = newAnimationTimeline()) {
    addAnimationToTimeline(timeline,
      newAnimation(this.element, 600, { translateY: [screen.height, 0] }))
    return timeline
  }

  /**
   * Animation to move onboarding leaflet and content in X axis
   * from outside screen to the center
   *
   * @param {AnimationTimeline} timeline
   * @return {AnimationTimeline}
   * @private
   */
  _revealTimeline(timeline = newAnimationTimeline()) {
    this._leafletTimeline(timeline)
    this._leftAssetsTimeline(timeline)
    this._rightAssetsTimeline(timeline)
    return timeline
  }

  async initializeView() {
  }

  async showView() {
    await playTimeline(this.viewTimeline, true)
    if(this.leafletController)
      setTimeout(() => this.leafletController.startLeaflet(), 1000)
  }

  async hideView() {
    await playTimeline(this.viewTimeline, true)
  }
}
