import ApplicationSerializer from '../application-serializer';
import GameModel from "../../models/game/game-model";
import CardModel from "../../models/game/deck/card-model";
import QuestionModel from "../../models/game/deck/question-model";
import AnswerModel from "../../models/game/deck/answer-model";
import DeckModel from "../../models/game/deck/deck-model";
import ResultsModel from "../../models/game/results-model";

/**
 * Public interface for conversion between mysenii API
 * Deck information and mysenii client game model
 */
export default class ApiGameSerializer extends ApplicationSerializer {
  /**
   * Parses API answer information into client Answer object
   *
   * @param {Object} apiAnswerJson API answer JSON object
   * @param {QuestionModel} question parsed question associated to answer
   * @return {AnswerModel}
   * @private
   */
  _parseApiAnswerJson(apiAnswerJson, question) {
    return new AnswerModel(
      apiAnswerJson.title,
      apiAnswerJson.description,
      apiAnswerJson.direction,
      apiAnswerJson.score,
      question
    )
  }

  /**
   * @param {Object} apiQuestionJson
   * @param {Object} apiGameJson
   * @return {QuestionModel}
   * @private
   */
  _parseApiQuestionJson(apiQuestionJson, apiGameJson) {
    const question = new QuestionModel(
      apiQuestionJson.question,
      apiQuestionJson.description,
      apiQuestionJson.categories,
      []
    )

    // Parse answers and assign them to question
    const apiGameJsonAnswers = apiGameJson.answers;
    apiGameJsonAnswers.forEach((a) => {
      const parsedAnswer = this._parseApiAnswerJson(a, question);
      question.addAnswer(parsedAnswer);
    });

    return question
  }

  /**
   *
   * @param {Object} apiCardJson
   * @param {Object} apiGameJson
   * @return {CardModel}
   * @private
   */
  _parseApiCardJson(apiCardJson, apiGameJson) {
    const cardQuestion = this._parseApiQuestionJson(apiCardJson, apiGameJson)
    const card = new CardModel(cardQuestion, apiCardJson.image, apiCardJson.color, apiCardJson.id)
    cardQuestion.card = card
    return card
  }

  /**
   *
   * @param {Object} apiGameJson
   * @return {DeckModel}
   * @private
   */
  _parseApiDeckJson(apiGameJson) {
    const apiGameCardsJson = apiGameJson.cards
    const cards = apiGameCardsJson.map((c) => this._parseApiCardJson(c, apiGameJson))
    return new DeckModel(cards);
  }

  /**
   *
   * @param apiGameJson
   * @return {ResultsModel[]}
   * @private
   */
  _parseApiResultsJson(apiGameJson) {
    const apiResultsJson = apiGameJson.results
    return apiResultsJson.map((r) => new ResultsModel(r.range, r.title, r.description))
  }

  /**
   *
   * @param {Object} apiGameJson
   * @return {GameModel}
   * @private
   */
  _parseApiGameJson(apiGameJson) {
    const deck = this._parseApiDeckJson(apiGameJson);
    const results = this._parseApiResultsJson(apiGameJson);
    return new GameModel(deck, results);
  }

  /**
   *
   * @param {Object} apiGameJson
   * @return {GameModel}
   */
  toGameModel(apiGameJson) {
    return this._parseApiGameJson(apiGameJson)
  }
}
