import PlugableController from '../common/plugable';
import { eventsHandler } from '../../mixins/events-handler';

/**
 * Scenes define self-contained modular parts of the application (like the gameplay,
 * the results menu, etc). The scene controller wraps each individual scene content
 * in a common logic that simplifies the task of transitioning between different modules
 * in the game, used by the master scene controller
 */
export default class SceneController extends PlugableController {
  static values = {
    // Scene name
    name: String,
    // Scene name of next scene to be rendered after present scene
    nextSceneName: String,
    // Event triggered by content controller when it is hidden/closed
    endSceneEvent: String,
    // Content controller method to hide/close it
    endSceneAction: String,
    // Event triggered by content controller when it is rendered/initialized
    startSceneEvent: String,
    // Content controller method to initialize/display it
    startSceneAction: String,
    // Event triggered by content controller when it can be displayed/registered
    registrationEvent: String,
    // Scene content controller can deliver information to next scene content
    // controller. Specifying the name of the attribute which contains the
    // information to share, the scene will extract the information from the
    // content controller and will pass it to the next scene via data value
    sceneContentControllerDataAttribute: String,
    // In case no data is injected from other scene default scene data can be
    // passed via this value
    defaultData: Object
  }

  // Scene Attributes
  sceneContentController = null

  initialize() {
    super.initialize()
    eventsHandler(this)

    if(!this._canInitialize()) return
    this._registerEventListeners()
  }

  /**
   * Initializes all controller listener events,
   * providing a clean way to have them organized
   *
   * @private
   */
  _registerEventListeners() {
    const listeners = [['loadData', (e) => { this.loadSceneData(e.detail) }]]

    if(this.hasRegistrationEventValue)
      listeners.push([this.registrationEventValue, (e) => { this._registerSceneContentController(e) }])
    if(this.hasStartSceneEventValue)
      listeners.push([this.startSceneEventValue, (e) => { this.startScene(e) }])
    if(this.hasEndSceneEventValue)
      listeners.push([this.endSceneEventValue, (e) => { this._notifySceneCompleted() }])

    listeners.forEach((l) => this.element.addEventListener(l[0], l[1]))
  }

  /**
   * Send notification to master scene to hide current
   * scene and load scene to next current scene
   *
   * @private
   */
  _notifySceneReady() {
    this.triggerEvent('sceneReady', this.element.parentElement, true, this)
  }

  /**
   * Send notification to master scene to load
   * next scene, hide current scene and show next one
   *
   * @private
   */
  _notifySceneCompleted() {
    this.triggerEvent('sceneCompleted', this.element.parentElement, true, this)
  }

  /**
   * Registers scene content controller in scene controller
   *
   * @param {CustomEvent} contentControllerInsertedEvent
   * @private
   */
  _registerSceneContentController(contentControllerInsertedEvent) {
    this.sceneContentController = contentControllerInsertedEvent.detail
    this._notifySceneReady()
  }

  /**
   * When content controller is registered in scene the initializer controller
   * method needs to be invoked to display the content controller. This method
   * takes care of calling this content controller logic to display it
   *
   * @param {*|null} previousSceneData information previous scene passes to current scene when
   *                                   being rendered
   * @return {Promise<void>}
   */
  async startScene(previousSceneData) {
    let sceneData = previousSceneData

    if(!sceneData && this.hasDefaultDataValue) sceneData = this.defaultDataValue

    if(this.hasStartSceneActionValue) {
      await this.sceneContentController[this.startSceneActionValue](sceneData)
    }
  }

  /**
   * When content controller notifies completion (for example game controller
   * notifies all cards were played) this method is invoked. The end method
   * will communicate with the master scene to hide the content controller and
   * prepare the next scene in the game logic
   *
   * @param endSceneEvent
   * @return {Promise<void>}
   */
  async endScene(endSceneEvent) {
    await this.sceneContentController[this.endSceneActionValue]()
  }

  /**
   * Replaces the value of the next scene to load after current scene completion
   *
   * @param {String} nextScene
   */
  switchNextScene(nextScene) {
    this.nextSceneNameValue = nextScene
  }

  /**
   * Information current scene will deliver to next scene after completion.
   * This information comes from the scene content controller and the attribute
   * specified in the data attribute value of the current scene controller
   *
   * @return {*|null}
   */
  nextSceneData() {
    return this.hasSceneContentControllerDataAttributeValue ?
      this.sceneContentController[this.sceneContentControllerDataAttributeValue] : null
  }
}
