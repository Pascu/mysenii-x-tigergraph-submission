import PlugableController from '../../../common/plugable';
import { domHandler } from '../../../../mixins/dom-handler';
import { flipHandler } from '../../../../mixins/leaflet/flip-handler';
import { eventsHandler } from '../../../../mixins/events-handler';
import { exceptionsHandler } from '../../../../mixins/exceptions-handler';

/**
 * Implements logic to generate and display interactive leaflets
 * in which usser can switch between pages via swipe mechanics
 */
export default class LeafletController extends PlugableController {
  // Tracks number of pages rendered
  static targets = ['pagesWrapper']
  static values = { leaflet: Object }

  // Leaflet
  pages = []
  topPage = null
  flippedPages = []

  initialize() {
    super.initialize()

    domHandler(this)
    flipHandler(this)
    eventsHandler(this)
    exceptionsHandler(this)

    this._registerEventListeners()
  }

  connect() {
    super.initialize()

    this._initializeFlip()
  }

  /**
   * Initializes all controller listener events,
   * providing a clean way to have them organized
   *
   * @private
   */
  _registerEventListeners() {
    // Collection of event names and corresponding listener callback method
    const listeners = [
      ['pageInserted', (e) => this._registerPage(e)],
      ['pageStartLeftSwipe', (e) => {  }],
      ['pageStartRightSwipe', (e) => { this.passToPreviousPage() }],
      ['currentPageFlippedBack', (e) => {}],
      ['currentPageFlippedForward', (e) => { this.passToNextPage() }],
      ['previousPageFlippedBack', (e) => {}],
      ['previousPageFlippedForward', (e) => {}],
    ]

    listeners.forEach((l) => this.element.addEventListener(l[0], l[1]))
  }

  /**
   * Checks if all pages have been registered in leaflet by comparing the number
   * of child elements inserted in pages wrapper with the number of registered pages
   *
   * @return {boolean}
   * @private
   */
  _leafletReady() {
    return this.pagesWrapperTarget.children.length === this.pages.length
  }

  /**
   * Registers page controller in leaflet. When all pages are
   * registered top page is assigned to leaflet to start scroll
   * and leaflet ready notification is sent
   *
   * @param {CustomEvent} pageInsertedEvent
   * @private
   */
  _registerPage(pageInsertedEvent) {
    this.pages.push(pageInsertedEvent.detail)
    if(this._leafletReady()) {
      this.topPage = this.lastSiblingPage()
      this.triggerEvent('leafletReady', this.element.parentElement, true, this)
    }
  }

  /**
   * Triggers animation to reveal leaflet is ready to be swiped.
   * Used by leaflet-based scenes as start action event
   */
  startLeaflet() {
    this.topPage.foldCorner()
  }

  /**
   * First leaflet page is the last DOM element inserted in the pages wrapper container,
   * covering the rest of pages. This method seeks for the last page controller corresponding
   * to the last page element in pages wrapper
   *
   * @return {PageController}
   */
  lastSiblingPage() {
    const topPage = this.pagesWrapperTarget.querySelector('[data-controller=app-leaflet-page]:last-child');
    if (!topPage) return null;
    return this.pages.find((p) => p.element.isSameNode(topPage));
  }

  /**
   * Page elements are piled in leaflet pages wrapper div in the same order they are
   * visualized in the UI. Given a page, we can infer previous and next pages by just
   * checking to the adjacent child elements
   *
   * @param {PageController} page
   * @return {*[]}
   */
  previousPage(page) {
    return this.pages.find((p) =>
      p.element.isSameNode(page.element.nextElementSibling)
    );
  }

  /**
   * Page elements are piled in leaflet pages wrapper div in the same order they are
   * visualized in the UI. Given a page, we can infer previous and next pages by just
   * checking to the adjacent child elements
   *
   * @param {PageController} page
   * @return {*[]}
   */
  nextPage(page) {
    return this.pages.find((p) =>
      p.element.isSameNode(page.element.previousElementSibling)
    );
  }

  /**
   * Selects next page in leaflet in order to be swiped.
   * This method is called when a card is flipped forward
   */
  passToNextPage() {
    this.canSwipe = false

    const currentPage = this.topPage
    this.flippedPages.push(currentPage)

    if(this.nextPage(currentPage)) {
      this.topPage = this.nextPage(currentPage)
      this.topPage.foldCorner()
      this.topPage.element.querySelector('.mysenii-page-flipper').parentElement
        .style.zIndex = this.flippedPages.length
      // TODO: move this line to the flip refactor
      this.lastRotation = 0
    }

    this.canSwipe = true
  }

  /**
   * Selects previous page in order to be swiped.
   * This method is called when a card is flipped back
   */
  passToPreviousPage() {
    this.canSwipe = false

    this.topPage.element.querySelector('.mysenii-page-flipper').parentElement
      .style.zIndex = 0
    this.topPage = this.flippedPages.pop()
    this.topPage.element.querySelector('.mysenii-page-flipper').parentElement
      .style.zIndex = this.flippedPages.length
    // TODO: move this line to the flip refactor
    this.lastRotation = -180

    this.canSwipe = true
  }
}
