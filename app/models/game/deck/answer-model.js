import ApplicationModel from './../../application-model';

/**
 * Models a game question answer
 */
export default class AnswerModel extends ApplicationModel{
  /**
   * Answer title
   * @type {String}
   */
  title;

  /**
   * Answer description
   * @type {String}
   */
  description;

  /**
   * Answer direction
   * @type {String}
   */
  direction;

  /**
   * Answer score
   * @type {number}
   */
  score;

  /**
   * Question answer is associated to
   * @type {Question}
   */
  question;

  constructor(title = '', description = '', direction = '', score = 0, question = null) {
    super();

    this.title = title;
    this.score = score;
    this.question = question;
    this.direction = direction;
    this.description = description;
  }

  get title() {
    return this.title;
  }

  get score() {
    return this.score;
  }

  get question() {
    return this.question;
  }

  get direction() {
    return this.direction;
  }

  get description() {
    return this.description;
  }

  /**
   * Returns question categories
   */
  get questionCategories() {
    return this.question.categories
  }

  get questionContent() {
    return this.question.content
  }

  /**
   * Returns the maximum answer score of the associated question
   */
  get questionMaximumScore() {
    return Math.max(...this.question.answers.map((a) => a.score))
  }

  /**
   * Returns the minimum answer score of the associated question
   */
  get questionMinimumScore() {
    return Math.min(...this.question.answers.map((a) => a.score))
  }

  get card() {
    return this.question.card;
  }

  set question(question) {
    this.question = question
  }
}
