import ApplicationModel from './../application-model';
import TestRunScoreModel from "./test-run-score-model";
import TestRunAnswerModel from "./test-run-answer-model";

export default class TestRunModel extends ApplicationModel{
  /**
   * @type {Array[TestRunAnswerModel]}
   */
  testRunAnswers;

  constructor(testRunAnswers = []) {
    super();

    this.testRunAnswers = testRunAnswers
  }

  /**
   *
   * @param {AnswerModel} answer
   * @private
   */
  _testRunAnswer(answer) {
    return new TestRunAnswerModel(answer)
  }

  /**
   *
   * @param testRunAnswer
   * @return {number}
   * @private
   */
  _testRunAnswerIndex(testRunAnswer) {
    return this.testRunAnswers.findIndex((tra) => tra.questionContent === testRunAnswer.questionContent)
  }

  /**
   * @return {Array[TestRunAnswerModel]}
   */
  get testRunAnswers() {
    return this.testRunAnswers;
  }

  /**
   * @return {Array[String]}
   */
  get testCategories() {
    const categories = this.testRunAnswers
      .map((tra) => tra.questionCategories)
      .flat()

    // Remove duplicated categories
    return Array.from(new Set(categories));
  }

  /**
   * @return {TestRunScoreModel}
   */
  get testRunScore() {
    return new TestRunScoreModel(this);
  }

  /**
   * return {number}
   */
  get testRunStandardScoreValue() {
    return this.testRunScore.standardScoreValue;
  }

  /**
   *
   * @param {String} category
   * @return {Array[TestRunAnswerModel]}
   */
  testRunCategoryAnswers(category) {
    return this.testRunAnswers.filter((tra) => {
      return tra.questionCategories.includes(category)
    })
  }

  /**
   *
   * @param {TestRunAnswerModel} testRunAnswer
   */
  addTestRunAnswer(testRunAnswer) {
    const testRunAnswerIndex = this._testRunAnswerIndex(testRunAnswer)

    if(testRunAnswerIndex === -1) {
      this.testRunAnswers.push(testRunAnswer);
    } else {
      // If question is already answered replace testRunAnswer
      this.testRunAnswers[testRunAnswerIndex] = testRunAnswer
    }
  }

  /**
   *
   * @param {AnswerModel} answer
   */
  addTestRunAnswerFromAnswer(answer) {
    this.addTestRunAnswer(this._testRunAnswer(answer))
  }
}
