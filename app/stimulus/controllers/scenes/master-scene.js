import { Controller } from '@hotwired/stimulus';
import { domHandler } from '../../mixins/dom-handler';
import { eventsHandler } from '../../mixins/events-handler';

export default class MasterSceneController extends Controller {
  static values = {
    // First scene to be rendered when master scene is rendered
    initialScene:String
  }

  static targets = ['scene', 'sceneWrapper']

  // Name of the current and next scenes in game. We use the names
  // to filter all scene controller actions coming from scenes that
  // are not the current and next scenes
  nextSceneName = null
  currentSceneName = null

  // Current scene controllers corresponding to the scene
  // names specified in the previous controller attribute
  currentScene = null

  swappingNextAndCurrentScenes = false

  initialize() {
    domHandler(this)
    eventsHandler(this)

    this._registerEventListeners()
  }

  /**
   * Initializes all controller listener events,
   * providing a clean way to have them organized
   *
   * @private
   */
  _registerEventListeners() {
    const listeners = [
      ['sceneReady', (e) => { this.swapNextAndCurrentScene(e.detail) }],
      ['sceneCompleted', (e) => { this.insertCurrentScenesNextScene(e.detail) }],
    ]

    listeners.forEach((l) => this.element.addEventListener(l[0], l[1]))
  }

  connect() {
    this._insertNextScene(this.initialSceneValue)
  }

  /**
   * Element where next scene content will be rendered
   *
   * @return {HTMLElement}
   * @private
   */
  _nextSceneWrapper() {
    return this.sceneWrapperTargets[0]
  }

  /**
   * Element where current scene content will be rendered
   *
   * @return {HTMLElement}
   * @private
   */
  _currentSceneWrapper() {
    return this.sceneWrapperTargets[1]
  }

  /**
   * Given a scene name this method seeks the associated DOM template element
   * where scene is rendered, copying the content and inserting it in the
   * specified wrapper element
   *
   * @param {String} sceneName
   * @param {HTMLElement} wrapperElement
   * @param {Object} sceneData
   * @private
   */
  _insertSceneInWrapper(sceneName, wrapperElement, sceneData = {}) {
    const sceneTemplate = this.element.querySelector(`template[data-scene-name=${sceneName}]`)
    this.insertTemplate(sceneTemplate, wrapperElement, true, sceneData)
  }

  /**
   * The master scenes uses two DIV elements to display the current and next scenes. These
   * elements are consecutive siblings with absolute positioning, meaning the last child in
   * the parent node childlist (where the current scene is rendered) covers the previous
   * child (where the next scene is rendered).
   *
   * This method swaps the position of both child, displaying the DIV element corresponding
   * to the next scene, which now also hides the current scene
   *
   * @private
   */
  _switchSceneWrappers() {
    this._nextSceneWrapper()
      .parentNode.insertBefore(this._currentSceneWrapper(), this._nextSceneWrapper());
  }

  /**
   * Renders scene in next scene wrapper
   *
   * @param {String} sceneName
   * @private
   */
  _insertNextScene(sceneName, sceneData = {}) {
    this.nextSceneName = sceneName
    const wrapper = this._nextSceneWrapper()
    this._insertSceneInWrapper(sceneName, wrapper, sceneData)
  }

  /**
   * Renders scene in current scene wrapper
   *
   * @param {String} sceneName
   * @private
   */
  _insertCurrentScene(sceneName, sceneData = {}) {
    this.currentSceneName = sceneName
    const wrapper = this._currentSceneWrapper()
    this._insertSceneInWrapper(sceneName, wrapper, sceneData)
  }

  /**
   * Invokes current scene start logic with previous current scene data
   *
   * @param previousCurrentSceneController
   * @return {Promise<void>}
   * @private
   */
  async _startCurrentScene(previousCurrentSceneController) {
    if(!this.currentScene) return

    // Extract scene data from old current scene to inject it in new current scene
    const previousCurrentSceneData = previousCurrentSceneController ? previousCurrentSceneController.nextSceneData() : null

    // Update references to current scene with new scene and trigger scene initialization logic
    await this.currentScene.startScene(previousCurrentSceneData)
  }

  /**
   * Invokes current scene end logic
   *
   * @return {Promise<void>}
   * @private
   */
  async _endCurrentScene() {
    if(this.currentScene)
      await this.currentScene.endScene(null)
  }

  /**
   * Invoked when a new scene is rendered. Implements the logic to transition from the current
   * scene to the next scenes, once both scenes are rendered (thus this method is invoked after
   * a scene is rendered).
   *
   * The method rans the hide animation of the current scene, after which it swaps next and current
   * scene wrappers position so the next scene is now the current scene, covering the previous current
   * scene. After swap start animation is run to reveal new current scene
   *
   * @param {Controller} renderedSceneController controller of scene rendered in DOM, represents new current scene
   * @return {Promise<void>}
   */
  async swapNextAndCurrentScene(renderedSceneController) {
    // This method is called on scene ready events. Scene ready events are triggered
    // when scenes are inserted in DOM. As side effect, when scenes DOM node is moved
    // in a different position of the page, the event is also triggered.
    // To avoid triggering multiple scene swaps during the event of swapping we use
    // this flag, which allow the method to only be triggered in the scenario of new
    // scenes being rendered
    if(!this.swappingNextAndCurrentScenes) {
      this.swappingNextAndCurrentScenes = true

      // Trigger current scene end animation
      await this._endCurrentScene()

      // Swap next and previous scene wrappers
      this._switchSceneWrappers()

      // Set new current scene controller and start it
      const previousCurrentScene = this.currentScene ? this.currentScene : null
      this.currentScene = renderedSceneController
      await this._startCurrentScene(previousCurrentScene)

      this.swappingNextAndCurrentScenes = false
    }
  }

  /**
   * Invoked when current scene notifies its completion to master scene.
   * Method extracts next scene to be rendered and data to pass to it
   * from current scene controller and renders it as a next scene
   *
   * @param {Controller} completedCurrentSceneController
   * @return {Promise<void>}
   */
  async insertCurrentScenesNextScene(completedCurrentSceneController) {
    // TODO: make sure completion event is triggered from current scene
    const nextSceneName = completedCurrentSceneController.nextSceneNameValue
    const nextSceneData = completedCurrentSceneController.nextSceneData()
    this._insertNextScene(nextSceneName, nextSceneData)
  }
}
