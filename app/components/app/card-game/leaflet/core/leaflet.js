import Component from '@glimmer/component';

export default class Leaflet extends Component {
  get stringifiedLeaflet() {
    return JSON.stringify(this.args.leaflet);
  }

  get pages() {
    return this.args.leaflet.pages.reverse();
  }

  get totalPages() {
    return this.args.leaflet.pages
      .filter((p) => ['introduction', 'content', 'back'].includes(p.type))
      .length;
  }
}
