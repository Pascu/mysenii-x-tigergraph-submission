import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

/**
 * Global controller configuration.
 */
export default class ApplicationController extends Controller {
  @service gameManager;

  // Controller Properties
  // ---------------------
  deck = null;

  /**
   * Load test and store it in local variable. This test will be the one
   * the game will load.
   */
  async loadDeck() {
    const deck = await this.gameManager.loadDeck();
    this.set('deck', deck);
  }

  /**
   * Initializes game application.
   * @return {Promise<void>}
   */
  async initializeGame() {
    await this.loadDeck();
  }

  /**
   * Called when application is executed. Checks whether user
   * is logged in. Deletes session if user didn't check the
   * "stay logged" option. If not, loads user profile in store.
   */
  init(...args) {
    super.init(args);
    this.initializeGame();
  }
}
