import Component from '@glimmer/component';

export default class Card extends Component {
  get stringifiedCard() {
    return JSON.stringify(this.args.card);
  }
}
