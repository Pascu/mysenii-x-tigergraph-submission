import ApplicationModel from './../application-model';
import AnswerModel from "../game/deck/answer-model";

export default class TestRunAnswerModel extends ApplicationModel{
  /**
   * @type {AnswerModel}
   */
  answer;

  /**
   * @type {Date}
   */
  timestamp;

  constructor(answer = new AnswerModel(), timestamp = Date.now) {
    super();
    this.answer = answer;
    this.timestamp = timestamp;
  }

  /**
   *
   * @return {AnswerModel}
   */
  get answer() {
    return this.answer;
  }

  /**
   * @return {Date}
   */
  get timestamp() {
    return this.timestamp;
  }

  get scoreValue() {
    return this.answer.score;
  }

  get maximumScoreValue() {
    return this.answer.questionMaximumScore;
  }

  get minimumScoreValue() {
    return this.answer.questionMinimumScore;
  }

  get questionCategories() {
    return this.answer.questionCategories;
  }

  get questionContent() {
    return this.answer.questionContent;
  }

  get card() {
    return this.answer.card;
  }
}
