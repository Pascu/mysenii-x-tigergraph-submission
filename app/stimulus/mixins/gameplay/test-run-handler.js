/**
 * Stimulus Controller Mixin for in-game score calculation logic.
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const testRunHandler = (controller) => {
  Object.assign(controller, {
    /**
     *
     * @param {TestRunModel} testRun
     * @param {GameModel} game
     * @param {CardController} gameCardController
     */
    addPlayedCardToTestRun(testRun, game, gameCardController) {
      const answerTitle = gameCardController.lastAnswer.title
      const questionContent = gameCardController.modelValue.question
      const selectedAnswer = game.answerByQuestionContentAndAnswerTitle(questionContent, answerTitle)
      testRun.addTestRunAnswerFromAnswer(selectedAnswer)
    }
  });
};
