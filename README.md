# TigerGraph Challenge - mysenii - Submission Repository

This is the mysenii team repository for the Tiger Graph 1 Million $ Challenge submission.

## Deliverables

During the preparation for the hackathon, our team has developed two applications to illustrate our problem statement, explain our proposed solution, and prove its feasibility and applicability in the mental healthcare system.

### mysenii Card Game Client

The mysenii Card Game Client (CGC) is a `Stimulus.js` & `ember.js` -based static web application. This web application implements the mysenii screening model format proposal, consisting of a deck of cards the user has to swipe through to complete the underlying test and automated screening results.

For our submission, we decided to apply our screening solution to a real psychological screening test, the **CES-DC**. Professionals use this test to identify depression symptoms in children and teenagers. Cards' illustrations were based on test questions, and screening results calculation was automated following the score formula of the test. To adapt the traditional test result to our multi-dimensional score approach, we categorized the **CES-DC** question into four categories, according to studies cited in the submission: depressive, interpersonal, positive, and somatic.

The application fully implements the test-based game with a dynamic score and simulated follow-up recommendations that explain how mysenii aims to build its customized patient journey. Due to the scope of the hackathon and the limitations in resources, some of the features displayed in the UI are incompleted/discarded from the current deliverable.

#### Demo Link
- [https://pascu.gitlab.io/mysenii-x-tigergraph-submission/](https://pascu.gitlab.io/mysenii-x-tigergraph-submission/)

#### Installation
##### Requirements
- Node.js v14.15.1 or higher.
- Ember CLI:
  - [Installation information can be found here.](https://cli.emberjs.com/release/)

##### Installation Steps
- Clone git repository.
- In your terminal window, go to the project folder.
- Run `npm install` to install the project dependencies.
- Run `ember s` to run our web application on a local server.
- Open your web browser, and navigate to [localhost:4200](http://localhost:4200/) to launch the application.
  - We recommend using `Google Chrome` or `Firefox` web browsers to test the application.

### mysenii CES-D x Tiger Graph Experiment

In the final vision of our solution, a backend application would connect our CGC application with a TigerGraph database, which models our user's patient journey proposal. The application would request user data to implement the business logic for early-identifying mental health conditions and recommend effective customized follow-up based on specific user information.

Due to the scope of the hackathon and the complexity of the proposed software solution, the team decided to work on **a software-based experiment that illustrates how this vision is technically feasible and scientifically validated in combination with the TigerGraph technologies**.

The mysenii `CES-D x TigerGrap` (CXT) is a `python`-based `Jupyter` notebook that illustrates how our platform could effectively detect potential cases of depression in patients and intervene to improve their conditions. The experiment uses actual patient CES-D depression scores, adapted to our multidimensional approach proposal, combined with time-series analysis prediction techniques to predict the conditions and the recommendation of follow-ups.

The notebook provides all the experiment data and steps to successfully emulate our graph-based database proposal, inject the actual patient data into it, and infer specific patient conditions via unique IDs. We complemented these steps with detailed step descriptions, graphs, images, and external references to help reviewers understand the logic behind the experiment.

#### Demo Link

To simplify the review of the experiment, we provide a `.pdf` document with the result of an example experiment applied to a random patient from our experiment dataset. This experiment can be replicated locally by following the instructions specified in the section below.

- [TigerGraph Database Modeling & Experiment Results](https://gitlab.com/Pascu/mysenii-x-tigergraph-submission/-/blob/master/ces-d-x-tigergraph/Experiment-Documentation.pdf)

#### TigerGraph Cloud Solution
You can find the TigerGraph exported solution under the `TigerGraph Solution` folder. Here there are all the documents and `.zip` file required by the submission rules to share our TG based solution with the judges.

#### Installation

##### Requirements
- [Python 3](https://www.python.org/downloads/) 
- [pip](https://packaging.python.org/en/latest/guides/tool-recommendations/#installation-tool-recommendations)
- [Jupyter](https://jupyter.org/install)

##### Requirements
- [Python 3](https://www.python.org/downloads/)
- [pip3](https://packaging.python.org/en/latest/guides/tool-recommendations/#installation-tool-recommendations)
- [Jupyter](https://jupyter.org/install)

##### Installation Steps
- Clone git repository.
- In your terminal window, go to the project folder.
- Navigate to `ces-d-x-tigergraph` folder.
- Make sure you have all required python packages installed in your environment.
  Run `pip3 install jupyter jupyterlab pandas datetime statsmodels faker matplotlib pyTigerGraph notebook`
- Run `jupyter notebook` in your terminal to launch a local Jypiter notebook server
  - If you receive a message of `command not found` in your terminal try running the command: `python3 -m notebook`
  - For other issues you can reach us at info@mysenii.com and we will be glad to help you with the technical issue.
- Open your web browser, and navigate to [localhost:8888](localhost:8888) to launch the notebook.
- Here you can find the files associated to the experiment. Each `.ipynb` file contains code associated to the modeling of our TigerGraph solution and the data processing for the identification and prediction of mental health diseases.
  - `DataExplorationAndSimulation.ipynb` represents the experiment applied over real patient data information for the detection of depression in users.
  - `TigerGraphMysenii.ipynb` represents the modeling of our patient journey data model and screening results in a graph-based TigerGraph database.
    - Tiger Graph Cloud Solution related files can be found under the folder `ces-d-x-tigergraph/tiger-graph-cloud-solution` folder of the present repository.
- Run the notebook cells to see how the code works.
