import ApplicationModel from './../application-model';

/**
 * Models Game Results description
 */
export default class ResultsModel extends ApplicationModel{
  /**
   * Range of scores corresponding to result object
   * @type {[number, number]}
   */
  range;

  /**
   * Results header
   * @type {String}
   */
  header;

  /**
   * Results description
   * @type {String}
   */
  description;

  /**
   * @param {[number, number]} range
   * @param {String} header
   * @param {String} description
   */
  constructor(range = [0,10], header = '', description = '') {
    super();
    this.range = range;
    this.header = header;
    this.description = description;
  }

  get range() {
    return this.range;
  }

  get header() {
    return this.header;
  }

  get description() {
    return this.description;
  }
}
