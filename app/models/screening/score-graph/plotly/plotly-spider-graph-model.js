import ApplicationModel from './../../../application-model';

/**
 * Models Plotty Spider Graph data
 */
export default class PlotlySpiderGraphModel extends ApplicationModel{
  data = [];

  layout = {
    autosize: false,
    width: 320,
    height: 320,
    showlegend: false,
    polar: {
      radialaxis: {
        visible: false,
      }
    },
    font: {
      size: 7,
    }
  };

  constructor(testRunScores = [], customizedLayout = {}) {
    super();
    Object.assign(this.layout, customizedLayout)
    this.data = this._testRunScoresData(testRunScores)
  }

  _mockPreviousScoreData() {
  }

  _mockAverageScoreData() {
    return {
      type: 'scatterpolar',
      r: [0.9, 0.85, 0.95, 0.97],
      theta: ['Positive', 'Interpersonal', 'Depressive', 'Somatic'],
      mode: 'none',
      fill: 'toself',
      fillcolor: 'rgba(255, 222, 157, 0.4)',
      marker: { size: 0 },
      name: 'Average Score',
    }
  }

  _mockPreviousScoreData() {
    return {
      type: 'scatterpolar',
      r: [0.4, 0.95, 0.65, 0.77],
      theta: ['Positive', 'Interpersonal', 'Depressive', 'Somatic'],
      mode: 'none',
      fill: 'toself',
      fillcolor: 'rgba(224, 182, 252, 0.4)',
      marker: { size: 0 },
      name: 'Average Score',
    }
  }

  /**
   *
   * @param {TestRunScoreModel} testRunScore
   * @private
   */
  _testRunScoreData(testRunScore) {
    const normalizedPyschosomaticScore = testRunScore.normalizedPsychosometricScore

    return {
      type: 'scatterpolar',
      r: Object.values(normalizedPyschosomaticScore),
      theta: Object.keys(normalizedPyschosomaticScore),
      mode: 'none',
      fill: 'toself',
      fillcolor: 'rgba(255, 142, 115, 0.6)',
      marker: { size: 0 },
      name: 'Current Score',
    }
  }

  /**
   *
   * @param {TestRunScoreModel[]} testRunScores
   * @return {Object}
   * @private
   */
  _testRunScoresData(testRunScores = []) {
    return [this._mockAverageScoreData(), this._mockPreviousScoreData()]
      .concat(testRunScores.map((trs) => this._testRunScoreData(trs)))
  }
}
