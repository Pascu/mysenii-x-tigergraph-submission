import Hammer from 'hammerjs';
import vendorPrefix from 'vendor-prefix';

/**
 * Card rotation calculation (in degrees). Uses card dimensions and movement coordinates in order
 * to calculate a rotation value that, when combined with translation transformation, creates a
 * better movement animation effect
 *
 * @param cardElement
 * @param offsetX
 * @param offsetY
 * @param maxRotation
 * @return {number}
 */
function defaultRotationTransformation(
  cardElement,
  offsetX,
  offsetY,
  maxRotation = 15
) {
  const horizontalOffset = Math.min(
    Math.max(offsetX / cardElement.offsetWidth, -1),
    1
  );
  const verticalOffset =
    (offsetY > 0 ? 1 : -1) * Math.min(Math.abs(offsetY) / 100, 1);
  return horizontalOffset * verticalOffset * maxRotation;
}

/**
 * Card movement CSS transformation in one animation frame. Run this transformation
 * multiple times while swiping to create a smooth effect of card beign dragged
 *
 * @param cardElement
 * @param offsetX
 * @param offsetY
 */
function defaultMovementTransformation(cardElement, offsetX, offsetY) {
  const rotation = defaultRotationTransformation(cardElement, offsetX, offsetY);
  cardElement.style[
    vendorPrefix('transform')
  ] = `translate3d(0, 0, 0) translate(${offsetX}px, ${offsetY}px) rotate(${rotation}deg)`;
}

/**
 * Page flip CSS transformation in one animation frame. Run this transformation
 * multiple times while swiping to create a smooth effect of page beign flippled
 *
 * @param pageElement
 * @param rotation
 */
function defaultPageFlipTransformation(pageElement, rotation) {
  pageElement.style[
    vendorPrefix('transform')
  ] = `rotateY(${rotation}deg)`;
}

/**
 * Logic to flip page via custom animation
 *
 * @param pageElement
 * @param pageRotation
 * @param swipeRotation
 * @param flipTransformation
 */
function flipPage(pageElement, pageRotation, swipeRotation, flipTransformation = defaultPageFlipTransformation) {
  if(pageRotation === swipeRotation) return
  const rotation = swipeRotation
  return flipTransformation(pageElement, rotation)
}

/**
 * Logic to move card. Infers new card position via the current card offset
 * and the movement offset (so original card displacement + movement displacement)
 * and uses it to trigger a custom movement animation
 *
 * @param { HTMLElement } cardElement HTML element corresponding to card in movement
 * @param { Number } cardOffsetX original card X offset from center
 * @param { Number } cardOffsetY original card Y offset from center
 * @param { Number } movementOffsetX movement offset from cardOffsetX
 * @param { Number } movementOffsetY movement offset from cardOffsetX
 * @param { Function } movementTransformation function that applies a CSS transformation to element to simulate movement
 */
function moveCard(
  cardElement,
  cardOffsetX,
  cardOffsetY,
  movementOffsetX,
  movementOffsetY,
  movementTransformation = defaultMovementTransformation
) {
  if (cardOffsetX === movementOffsetX && cardOffsetY === movementOffsetY)
    return;

  const offsetX = cardOffsetX + movementOffsetX;
  const offsetY = cardOffsetY + movementOffsetY;

  return movementTransformation(cardElement, offsetX, offsetY);
}

/**
 * Set Hammer listerners for card pan (swipe) events
 *
 * @param { HTMLElement } cardElement HTML element to initialize with Hammer.JS
 * @param { Number } panThreshold pixels to swipe to trigger pan detection
 * @param { Function } onPanStart pan start callback
 * @param { Function } onPanMove pan move callback
 * @param { Function } onPanEnd pan end move callback
 * @return {Manager}
 */
function cardHammerConfiguration(
  cardElement,
  panThreshold = 1,
  onPanStart,
  onPanMove,
  onPanEnd,
  enableCallback = () => true
) {
  // Set of recognized Hammer.js events
  const hammerManager = new Hammer.Manager(cardElement, {
    recognizers: [
      // Only recognize pointer-down + movement events, specify threshold to trigger events.
      [Hammer.Pan, { threshold: panThreshold, enable: enableCallback }],
    ],
  });

  // Set Hammer.js listeners
  hammerManager.on('panstart', (event) => {
    onPanStart(event);
  });
  hammerManager.on('panmove', (event) => {
    onPanMove(event);
  });
  hammerManager.on('panend', (event) => {
    onPanEnd(event);
  });

  return hammerManager;
}

export {
  defaultRotationTransformation,
  defaultMovementTransformation,
  defaultPageFlipTransformation,
  flipPage,
  moveCard,
  cardHammerConfiguration,
};
