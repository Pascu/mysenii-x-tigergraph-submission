# Changelog

mysenii APP official Changelog.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.8] - 2022-04-04

## [0.0.7] - 2022-04-04
### Added
- Scenes now can exchange information between them.
- Scenes can have now dynamic flows (scenes can change next scene value).
- Dynamic game scores with score based follow ups.
- User game answers modelled in Test Run model.
- Dynamic scene rendering with handlebars.

## [0.0.6] - 2022-04-01
### Added
- This is the first officially documented version of the project.
- It implements the basic game flow from welcome screen to test results.
