import anime from 'animejs';

/**
 * Creates new animation object compatible with Anime.js
 *
 * @param {Array[HTMLElement]} targets HTML elements where animation is applied
 * @param {Number} duration animation length (ms)
 * @param {Object} transformation Anime.js transformation (https://animejs.com/documentation/#CSStransforms)
 * @param {String} easing animation easing (https://animejs.com/documentation/#linearEasing)
 * @param {Function} begin_callback animation start callback
 * @param {Function} completed_callback animation end callback
 * @param {Function} updated_callback animation end callback
 * @return {{duration: number, complete: completed_callback, targets: *[], easing: string, begin: begin_callback}}
 */
function newAnimation(
  targets = [],
  duration = 0,
  transformation = {},
  easing = 'linear',
  begin_callback = null,
  completed_callback = null,
  updated_callback = null) {
  // Merge transformation parameters with animation parameters for final animation
  const newAnimation = Object.assign(
    {
      targets,
      duration,
      easing,
      begin: begin_callback,
      complete: completed_callback,
      update: updated_callback,
      autoplay: false,
    },
    transformation);

  // Remove empty attributes
  Object.keys(newAnimation).forEach(k => { if(newAnimation[k] === null) delete newAnimation[k] })

  return newAnimation
}

/**
 * Create new timeline of animations
 *
 * @param {Array[Object]} animations collection of animations in cronological order to be loaded in timeline
 * @param {String|Number} offset wait time between animations (https://animejs.com/documentation/#timelineOffsets)
 * @param {AnimationTimeline} timeline optional timeline parameter used to add animations to an existing timeline
 *                                     instead of creating a new one
 * @return {AnimationTimeline}
 */
function newAnimationTimeline(animations = [], offset = 0, timeline = anime.timeline({ autoplay: false })) {
  animations.forEach((a, i) => timeline.add(a, offset))
  return timeline
}

/**
 * Add new animation to existing timeline
 *
 * @param {AnimationTimeline} timeline timeline of animations to be updated
 * @param {Object} animation new animation to add at the end of timeline timeline
 * @param {String|Number} offset wait time between this and rest of animations (https://animejs.com/documentation/#timelineOffsets)
 * @return {*}
 */
function addAnimationToTimeline(timeline, animation, offset = 0) {
  timeline.add(animation, offset)
  return timeline
}

/**
 * Run single animation
 *
 * @param {Object} animation animation to play
 * @return {*}
 */
async function playAnimation(animation) {
  const tl = anime.timeline()
  addAnimationToTimeline(tl, animation)
  tl.play()
  return tl
}

/**
 * Generic logic to play a timeline, with different
 * options to trigger animation pause, rewind and loops
 *
 * @param {AnimationTimeline} timeline
 * @param {Boolean} rewind if true reverts current animation and plays it
 *                         backwards until it reaches the original status
 * @return {Promise<*>}
 */
async function playTimeline(timeline, rewind = false) {
  if(rewind && timeline.began) {
    if(!timeline.completed) timeline.pause()
    timeline.reverse()
  }

  timeline.play();
  await timeline.finished
  return timeline
}

function invertTimeline(timeline) {
  timeline.seek(timeline.duration)
  timeline.reverse()
  return timeline
}

export { newAnimation, newAnimationTimeline, addAnimationToTimeline, playAnimation, playTimeline, invertTimeline }
