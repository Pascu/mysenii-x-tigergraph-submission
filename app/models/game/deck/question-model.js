import ApplicationModel from './../../application-model';

/**
 * Models a game question
 */
export default class QuestionModel extends ApplicationModel{
  /**
   * Question text
   * @type {String}
   */
  content;

  /**
   * Question description. Provides further insight of
   * the content for better understanding of its meaning
   * @type {String}
   */
  clarification;

  /**
   * Collection of psychosometric properties associated to question
   * @type {Array[String]}
   */
  categories;

  /**
   * Collection of answers associated to a question
   * @type {Array[AnswerModel]}
   */
  answers;

  /**
   * Card associated to question
   * @type {CardModel}
   */
  card;

  constructor(content = '', clarification = '', categories = [], answers = [], card = null) {
    super();

    this.card = card;
    this.content = content;
    this.answers = answers;
    this.categories = categories;
    this.clarification = clarification;
  }

  get content() {
    return this.content
  }

  get clarification() {
    return this.clarification
  }

  get categories() {
    return this.categories
  }

  get answers() {
    return this.answers
  }

  get card() {
    return this.card;
  }

  set card(card) {
    this.card = card
  }

  addAnswer(answer) {
    this.answers.push(answer)
  }

  /**
   *
   * @param {String} answerTitle
   * @return {AnswerModel}
   */
  answerByTitle(answerTitle) {
    return this.answers.find((a) => a.title === answerTitle)
  }
}
