import ApplicationModel from './../application-model';

/**
 * Our tests have multidimensional scores. Each score is
 * represented by a value and the maximum and minimum
 * values a score can have.
 */
export default class ScoreModel extends ApplicationModel{
  /**
   * Numeric score value
   * @type {number}
   */
  scoreValue;

  /**
   * Maximum value the score can have
   * @type {number}
   */
  maximumScoreValue = 100;

  /**
   * Minimum value the score can have
   * @type {number}
   */
  minimumScoreValue = 0;

  constructor(score = 0, minimumValue = 0, maximumValue = 100) {
    super();

    this.scoreValue = score;
    this.minimumScoreValue = minimumValue;
    this.maximumScoreValue = maximumValue;
  }

  /**
   * @return {number}
   */
  get scoreValue() {
    return this.scoreValue;
  }

  /**
   * @return {number}
   */
  get maximumScoreValue() {
    return this.maximumScoreValue;
  }

  /**
   * @return {number}
   */
  get minimumScoreValue() {
    return this.minimumScoreValue;
  }

  /**
   * @return {Array[String]}
   */
  get questionCategories() {
    return this.question.categories;
  }

  /**
   * @return {number}
   */
  get normalizedScoreValue() {
    return (1/this.maximumScoreValue) * this.scoreValue
  }
}
