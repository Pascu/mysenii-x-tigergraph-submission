import Component from '@glimmer/component';

export default class Tutorial extends Component {
  get stringifiedGame() {
    return JSON.stringify(this.args.deck);
  }

  get cards() {
    return this.args.deck.tutorial.cards;
  }
}
