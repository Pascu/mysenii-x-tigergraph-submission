import { Controller } from '@hotwired/stimulus';
import { eventsHandler } from '../../mixins/events-handler';
import {newAnimation} from "../../../utils/animations/animation-methods";

/**
 * Implements deck logic
 */
export default class DeckController extends Controller {
  // Deck
  cards = [];
  topCard = null;

  initialize() {
    eventsHandler(this);

    // Listens to cards being rendered to register them
    this.element.addEventListener('cardInserted', (e) => this._registerCard(e));
  }

  connect() {
    // Notify deck has been inserted
    this.triggerEvent('deckInserted', this.element.parentElement, true, this);
  }

  /**
   * Checks if all cards have been registered in deck by comparing the number
   * of child elements with the number of registered cards.
   *
   * @return {boolean}
   * @private
   */
  _deckReady() {
    return this.element.children.length === this.cards.length;
  }

  /**
   * Logic triggered when cards are rendered. Register card in cards attribute
   * and check whether all cards have been registered. If this is the case
   * trigger event indicating deck is ready.
   *
   * @param cardInsertedEvent
   * @private
   */
  _registerCard(cardInsertedEvent) {
    this.cards.push(cardInsertedEvent.detail);
    if (this._deckReady())
      this.triggerEvent('deckReady', this.element.parentElement, true, this);
  }

  zoomOutAnimation(scale, duration) {
    return newAnimation(this.element, duration, { scale }, 'easeOutElastic(.2, 1)')
  }

  /**
   * Deck element siblings represent cards. Last child is rendered over all
   * remaining siblings, representing the top card when game is rendered.
   * This method returns the card controller associated to the last sibling
   * element to help initialize the game
   *
   * @return {CardController}
   */
  lastSiblingCard() {
    const topCard = this.element.querySelector(
      '[data-controller=gameplay-card]:last-child'
    );
    if (!topCard) return null;
    return this.cards.find((c) => c.element.isSameNode(topCard));
  }

  /**
   * Given a card returns the rest of cards in deck
   *
   * @param {Card} card
   * @return {*[]}
   */
  remainingCards(card) {
    if (!card) return [];
    return this.cards.filter((c) => !c.element.isSameNode(card.element));
  }

  /**
   * Card elements are piled in deck parent div in the same order they are visualized in
   * the UI. Given a card we can infer previous and next cards by just checking to the
   * adjacent child elements
   *
   * @param {CardController} card
   * @return {*[]}
   */
  previousCard(card) {
    return this.cards.find((c) =>
      c.element.isSameNode(card.element.nextElementSibling)
    );
  }

  /**
   * Card elements are piled in deck parent div in the same order they are visualized in
   * the UI. Given a card we can infer previous and next cards by just checking to the
   * adjacent child elements
   *
   * @param {CardController} card
   * @return {*[]}
   */
  nextCard(card) {
    return this.cards.find((c) =>
      c.element.isSameNode(card.element.previousElementSibling)
    );
  }
}
