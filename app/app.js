import Resolver from 'ember-resolver';
import Application from '@ember/application';
import config from 'mysenii-app/config/environment';
import loadInitializers from 'ember-load-initializers';

// Stimulus Dependency & Project Controllers
// -----------------------------------------
import { Application as StimulusApplication } from '@hotwired/stimulus';
import UIController from './stimulus/controllers/gameplay/ui/ui';
import SceneController from './stimulus/controllers/scenes/scene';
import CardController from './stimulus/controllers/gameplay/card';
import DeckController from './stimulus/controllers/gameplay/deck';
import GameController from './stimulus/controllers/gameplay/game';
import HelpController from './stimulus/controllers/gameplay/ui/help';
import AnswerPromptController from './stimulus/controllers/gameplay/card/answer-prompt';
import MasterSceneController from './stimulus/controllers/scenes/master-scene';
import SplashController from './stimulus/controllers/app/splash/splash';
import LeafletController from './stimulus/controllers/app/card-game/leaflet/leaflet';
import PageController from './stimulus/controllers/app/card-game/leaflet/page';
import CardExplainerController from './stimulus/controllers/app/card-game/results/sections/card_explainer_controller';
import OpacitySplashController from "./stimulus/controllers/app/splash/opacity_splash";
import OnboardingLeafletController from "./stimulus/controllers/app/card-game/views/onboarding_leaflet_controller";
import GlideController from './stimulus/controllers/common/glide_controller';
import ResultsController from "./stimulus/controllers/app/card-game/views/results_controller";
import TestExplainerController from "./stimulus/controllers/app/card-game/views/test_explainer_controller";
import CardFlipController from "./stimulus/controllers/common/card_flip_controller";

// Initialize Ember Application
// ----------------------------
export default class App extends Application {
  modulePrefix = config.modulePrefix;
  podModulePrefix = config.podModulePrefix;
  Resolver = Resolver;
}

loadInitializers(App, config.modulePrefix);

// Initialize Stimulus & Controllers
// ---------------------------------
window.Stimulus = StimulusApplication.start();

// Common Components
// =================
Stimulus.register('common-card-flip', CardFlipController)

// Glider.js Slider
// ----------------
Stimulus.register('common-glide', GlideController)

// Application Master-Child Scene System
// -------------------------------------
Stimulus.register('scenes-scene', SceneController)
Stimulus.register('scenes-master-scene', MasterSceneController)

Stimulus.register('gameplay-ui-ui', UIController)
Stimulus.register('gameplay-card', CardController)
Stimulus.register('gameplay-deck', DeckController)
Stimulus.register('gameplay-game', GameController)
Stimulus.register('gameplay-ui-help', HelpController)
Stimulus.register('gameplay-card-answer-prompt', AnswerPromptController)

Stimulus.register('app-splash', SplashController)
Stimulus.register('app-opacity-splash', OpacitySplashController)

Stimulus.register('app-leaflet', LeafletController)
Stimulus.register('app-leaflet-page', PageController)

Stimulus.register('app-card-game-results-card-explainer', CardExplainerController)

// Application Views
// =================
// Card Game Views
// ---------------
Stimulus.register('app-card-game-views-onboarding-leaflet', OnboardingLeafletController)
Stimulus.register('app-card-game-views-test-explainer', TestExplainerController)
Stimulus.register('app-card-game-views-results', ResultsController)

// Application View Controllers
// ============================
