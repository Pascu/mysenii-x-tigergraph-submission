/**
 * Stimulus Controller Mixin for managing DOM (adding, removing, copying elements).
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const exceptionsHandler = (controller) => {
  Object.assign(controller, {
    /**
     * Single APP point logic to trigger exceptions
     *
     * @param {String} controllerName
     * @param {String} message
     */
    throwException(controllerName = '',message = 'Custom exception triggered') {
      throw(`${controllerName}: ${message}`)
    }
  });
};
