/**
 * List of possible directions to swipe
 *
 * @type {{DOWN: symbol, DOWNRIGHT: symbol, LEFT: symbol, UPLEFT: symbol, RIGHT: symbol, DOWNLEFT: symbol, UPRIGHT: symbol, UP: symbol}}
 */
const Directions = {
  UP: 'UP',
  DOWN: 'DOWN',
  LEFT: 'LEFT',
  RIGHT: 'RIGHT',
  UPLEFT: 'UPLEFT',
  UPRIGHT: 'UPRIGHT',
  DOWNLEFT: 'DOWNLEFT',
  DOWNRIGHT: 'DOWNRIGHT',
};

const DirectionDestinations = {
  UP: [0, -1],
  DOWN: [0, 1],
  LEFT: [-1, 0],
  RIGHT: [1, 0],
  UPLEFT: [-1, -1],
  UPRIGHT: [1, -1],
  DOWNLEFT: [-1, 1],
  DOWNRIGHT: [1, 1],
};

function calculateDirectionDestination(direction, cardElement, boardElement) {
  if (!Directions.hasOwnProperty(direction.toString())) return [0, 0];
  const xFactor = cardElement.offsetWidth / 2 + boardElement.offsetWidth / 2;
  const yFactor = cardElement.offsetHeight / 2 + boardElement.offsetHeight / 2;
  const destinationCoordinates = DirectionDestinations[direction];

  return [
    destinationCoordinates[0] * xFactor,
    destinationCoordinates[1] * yFactor,
  ];
}

/**
 * Calculates left-right swipe direction
 *
 * @param movementOffsetX
 * @param movementOffsetY
 * @return {symbol}
 */
function horizontalSwipe(movementOffsetX, movementOffsetY) {
  return movementOffsetX < 0 ? Directions['LEFT'] : Directions['RIGHT'];
}

/**
 * Calculates top-down swipe direction
 *
 * @param movementOffsetX
 * @param movementOffsetY
 * @return {symbol}
 */
function verticalSwipe(movementOffsetX, movementOffsetY) {
  return movementOffsetY < 0 ? Directions['UP'] : Directions['DOWN'];
}

/**
 * Calculates 4 axis swipe direction
 *
 * @param {Number} movementOffsetX
 * @param {Number} movementOffsetY
 * @return {Function} corresponding method for calculating movement direction
 */
function crossAxisSwipe(movementOffsetX, movementOffsetY) {
  return Math.abs(movementOffsetX) > Math.abs(movementOffsetY)
    ? horizontalSwipe(movementOffsetX, movementOffsetY)
    : verticalSwipe(movementOffsetX, movementOffsetY);
}

/**
 * Calculates drag movement direction via offset
 *
 * @param {Number} directions number of recognized directions (2-left/right, 4-top/down/left/right)
 * @param {Number} movementOffsetX
 * @param {Number} movementOffsetY
 * @return {symbol} calculated direction
 */
function calculateDirection(directions, movementOffsetX, movementOffsetY) {
  switch (directions) {
    case 2:
      return horizontalSwipe(movementOffsetX, movementOffsetY);
    case 4:
      return crossAxisSwipe(movementOffsetX, movementOffsetY);
  }
}

export { calculateDirection, calculateDirectionDestination };
