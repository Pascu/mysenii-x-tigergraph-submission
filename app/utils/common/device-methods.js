/**
 * Is game device interface a touch screen?
 *
 * @return {boolean|*}
 */
function isTouchDevice() {
  return 'ontouchstart' in window || navigator.msMaxTouchPoints;
}

export { isTouchDevice };
