/**
 * Stimulus Controller Mixin for generating random numbers between range
 * https://www.betterstimulus.com/architecture/mixins.html
 * @param {Controller} controller
 */
export const randomizerHandler = (controller) => {
  Object.assign(controller, {
    /**
     * Generate a random number between from and to
     *
     * @param {Number} from
     * @param {Number} to
     * @return {*} Random number between from and to
     */
    randomNumber(from, to) {
      return Math.floor(Math.random() * to) + from;
    },
  });
};
